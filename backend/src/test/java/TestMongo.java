import DAO.DAO_Impl_Mongo;
import POJO.Film;
import POJO.Person;
import POJO.Ticket;

import java.util.Date;
import java.util.List;

public class TestMongo {
    public static void main(String[] args) {
        DAO_Impl_Mongo dao = new DAO_Impl_Mongo();
        dao.dropAll();
        Person p0 = new Person("dummy" + 0 + "@gmail.com", "DummyFirstname" + 0, "DummySurname" + 0, "DummyCounrty" + 0, "DummyLocation" + 0, "DummyCountry" + 0, "DummyStreet" + 0, "DummyHash" + 0);
        Person p1 = new Person("dummy" + 1 + "@gmail.com", "DummyFirstname" + 1, "DummySurname" + 1, "DummyCounrty" + 1, "DummyLocation" + 1, "DummyCountry" + 1, "DummyStreet" + 1, "DummyHash" + 1);
        Person p2 = new Person("dummy" + 2 + "@gmail.com", "DummyFirstname" + 2, "DummySurname" + 2, "DummyCounrty" + 2, "DummyLocation" + 2, "DummyCountry" + 2, "DummyStreet" + 2, "DummyHash" + 2);
        p0.setFriend(p1);
        p0.setFriend(p2);
        p1.setFriend(p2);

        p0.addTicket(new Ticket("123456",new Date(),new Film(),p0));
        p0.addTicket(new Ticket("123321",new Date(),new Film(),p0));

        try {
            dao.createPerson(p0);
            dao.createPerson(p1);
            dao.createPerson(p2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Person> personList = dao.getallPersons();
        System.out.println();
        Ticket tickettodelete = personList.get(0).getTickets().get(0);
        dao.deleteTicket(tickettodelete);
        dao.getallPersons();
        System.out.println("Done");
    }
}
