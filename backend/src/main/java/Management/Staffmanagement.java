package Management;

import DAO.DAO_Impl;
import DAO.DAO;
import DAO.DAO_Proxy;
import POJO.Staff;

public class Staffmanagement {

    private static Staffmanagement instance;
    DAO_Proxy proxy = DAO_Proxy.getInstance();

    public static Staffmanagement getInstance () {
        if (Staffmanagement.instance == null) {
            Staffmanagement.instance = new Staffmanagement();
        }
        return Staffmanagement.instance;
    }

    /*
        Constructor of the class Staffmanagement
     */
    public Staffmanagement(){

    }
    /*
    add staff to the database
 */
    public String createStaff (Staff staff){
        try {
            return proxy.getDao().createStaff(staff);
        } catch (Exception e) {
            return null;
        }
    }

    /*
        Update the staff personal
     */
    public void updateStaff (Staff staff){
        proxy.getDao().updateStaff(staff);
    }

    /*
        Delete the staff in the cinema
     */
    public void deleteStaff (Staff staff){
        proxy.getDao().deleteStaff(staff);
    }

    /*
        Set chef manager of the staff
     */
    public void setCheffromStaff(Staff staff){

    }
}
