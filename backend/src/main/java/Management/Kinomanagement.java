package Management;

import DAO.DAO;
import DAO.DAO_Proxy;
import DAO.DAO_Impl;
import POJO.Kino;
import POJO.Owner;
import POJO.Saal;
import POJO.Staff;
import Webservice.ApiResponseMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ws.rs.core.Response;
import java.lang.reflect.Modifier;
import java.util.List;

public class Kinomanagement {

    DAO_Proxy proxy = DAO_Proxy.getInstance();
    private static Kinomanagement instance;
    Gson gson;

    /**
     * Classical Singleton implementation
     */
    public static Kinomanagement getInstance () {
        if (Kinomanagement.instance == null) {
            Kinomanagement.instance = new Kinomanagement();
        }
        return Kinomanagement.instance;
    }

    private Kinomanagement(){
        gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PROTECTED).create();
    }


    public Response addKino(Kino kino  ){
        String id = proxy.getDao().createKino(kino);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, id)).build();
    }

    public Response removeKino(String kinoId){
        Kino kino = proxy.getDao().getKinoById(kinoId);
        proxy.getDao().deleteKino(kino);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, kinoId)).build();
    }


    public Response updateKino(Kino kino  )  {
        proxy.getDao().updateKino(kino);
        String json = gson.toJson(kino);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, json)).build();
    }

    public Response findKinoById(String kinoId  )  {
        Kino kino = proxy.getDao().getKinoById(kinoId);
        String kinoJSON = gson.toJson(kino);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, kinoJSON)).build();
    }

    public static Kino setSaalsToNull(Kino kino) {
        for (Saal s : kino.getRooms()){
            s.setFilmList(null);
            s.setKinolocation(null);
        }
        return kino;
    }

    public Response findSaalsOfKino(String kinoId) {
        List<Saal> list = proxy.getDao().getSaalsofKinobyKinoID(kinoId);
        String saals = gson.toJson(list);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, saals)).build();
    }

    public Response findAllKinos() {
        List<Kino> kino = proxy.getDao().getallKinos();
        String gsontest = null;
        if (kino == null){
            return Response.noContent().build();
        }
        gsontest = gson.toJson(kino);

        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, gsontest)).build();
    }

    public Response addSaal(String kinoId, Saal body) {
        Kino kino = proxy.getDao().getKinoById(kinoId);
        body.setKinolocation(kino);
        proxy.getDao().updateKino(kino);
        proxy.getDao().createSaal(body);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, String.valueOf(body.getSaalnumber()))).build();
    }

    public Response findOwnerOfKino(String kinoId) {
        List<Owner> ownerList = proxy.getDao().getOwnersOfKinobyKinoID(kinoId);
        String gsonOwner = gson.toJson(ownerList);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, gsonOwner)).build();
    }

    public Response findStaffOfKino(String kinoId) {
        List<Staff> staffList = proxy.getDao().getStaffOfKinobyKinoID(kinoId);
        String gsonStaff = gson.toJson(staffList);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, gsonStaff)).build();

    }
}
