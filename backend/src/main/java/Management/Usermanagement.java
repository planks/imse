package Management;

import DAO.DAO;
import DAO.DAO_Proxy;
import DAO.DAO_Impl;
import POJO.Person;
import POJO.Ticket;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Usermanagement {

    private static Usermanagement instance;
    DAO_Proxy proxy = DAO_Proxy.getInstance();
    private Authentication authentication;
    /*
       Create a Singleton Pattern Usermanagement
     */
    public static Usermanagement getInstance () {
        if (Usermanagement.instance == null) {
            Usermanagement.instance = new Usermanagement();
        }
        return Usermanagement.instance;
    }
    /*
        Constuctor of the class Usermanagement
     */
    public Usermanagement(){
        authentication=new Authentication();
    }

    /*
        Add the person into the database after registration
    */
    public String createPerson (Person person){
        try {
            return proxy.getDao().createPerson(person);
        } catch (Exception e) {
            return null;
        }
    }

    /*
        Update the person
     */
    public void updatePerson(Person person){
        try {
            proxy.getDao().updatePerson(person);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
        Delete the person with the parameters
     */
    public void deletePerson (Person person){
        proxy.getDao().deletePerson(person);
    }

    public List<Ticket> getTickets(Person person){
        return person.getTickets();
    }

    /*
        Check the login of the User
     */
    public boolean loginUser(Person person){
        boolean isValid=false;
        List <Person> personList=proxy.getDao().getallPersons();
        for(int i=0; i<  personList.size();++i){
            if(person.getEmail().equals(personList.get(i).getEmail())&person.getPasswordhash().equals(personList.get(i).getPasswordhash())){
                isValid=true;
                break;
            }
        }
        return isValid;
    }

    public Person loginUser(String email,String password){
        List <Person> personList=proxy.getDao().getallPersons();
        for(Person p : personList){
            if(p.getEmail().equals(email)&&p.getPasswordhash().equals(password)){
                String token= authentication.generateToken();
                p.setToken(token);
                return p;
            }
        }
        return null;
    }

    /*
        Get the friends via the email
     */
    public Person searchFriendMail(String email){
        List <Person> personList=proxy.getDao().getallPersons();
        for(Person p : personList){
            if(p.getEmail().equals(email)){
                return p;
            }
        }
        return null;
    }

    public Person searchFriendName(String forename,String lastname){
        List <Person> personList=proxy.getDao().getallPersons();
        for(Person p : personList){
            if(p.getFirstname().equals(forename)&&p.getSurname().equals(lastname)){
                return p;
            }
        }
        return null;
    }

    /**
     * Erstellt einen zufälligen String
     * @return
     */
    public String createRandomString(){
        Random random =new Random(System.currentTimeMillis());
        long randomLong=random.nextLong();
        return Long.toHexString(randomLong);
    }

}
