package Management;

import POJO.Person;

import java.util.Random;

public class Authentication {

    Authentication(){}
    public String  generateToken(){
        Random random =new Random(System.currentTimeMillis());
        long randomLong=random.nextLong();
        return Long.toHexString(randomLong);
    }

    public boolean checkAccesstoEndpoint(String token, Person person){
        if(person.getToken().equals(token)){
          return true;
        }else return false;
    }

}
