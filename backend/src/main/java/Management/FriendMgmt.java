package Management;

import POJO.Person;

import java.util.ArrayList;
import java.util.List;

public class FriendMgmt {

    List<FriendMgmt> friends;
    List<String> temp;

    public  FriendMgmt(){
        temp=new ArrayList<String>();
        friends=new ArrayList<FriendMgmt>();
    }

    public String getFemail() {
        return femail;
    }

    public void setFemail(String femail) {
        this.femail = femail;
    }

    String femail;

    public String getMyMail() {
        return myMail;
    }

    public void setMyMail(String myMail) {
        this.myMail = myMail;
    }

    String myMail;

    public FriendMgmt(String friendEmail, String myMail){
        setFemail(friendEmail);
        setMyMail(myMail);
        friends=new ArrayList<FriendMgmt>();

    }

    public List<String> getFriends(String email){
        temp=new ArrayList<String>();
        if(friends!= null) {
            for (FriendMgmt friend : friends) {
                if(friend.getMyMail().equals(email)){
                    temp.add(friend.getFemail());
                }

            }
            return temp;
        }
        return null;
    }


    public void setFriend(FriendMgmt friend) {
        friends.add(friend);
    }

    public void removeFriend(FriendMgmt friend) { //thats so sad
        friends.remove(friend);
    }
}
