package Management;

import DAO.DAO;
import DAO.DAO_Proxy;
import DAO.DAO_Impl;
import POJO.Owner;
import POJO.Kino;
import POJO.Staff;

public class OwnerManagement {

    private static OwnerManagement instance;
    DAO_Proxy proxy = DAO_Proxy.getInstance();
    /*
       Create a Singleton Pattern for class OwnerManagement
     */
    public static OwnerManagement getInstance () {
        if (OwnerManagement.instance == null) {
            OwnerManagement.instance = new OwnerManagement();
        }
        return OwnerManagement.instance;
    }

    public OwnerManagement(){

    }

    /*
      Add owner into the database
   */
    public String createOwner (Owner owner){
        try {
            return proxy.getDao().createOwner(owner);
        } catch (Exception e) {
            return null;
        }
    }

    /*
        Update owner into  database
     */
    public void updateOwner (Owner owner){
        proxy.getDao().updateOwner(owner);
    }

    /*
        Delete owner from database
    */
    public void deleteOwner (Owner owner){
        proxy.getDao().deleteOwner(owner);
    }

    /*
     The owner create cinema and add it to the database
     */
    public String createKino (Kino kino){
       return proxy.getDao().createKino(kino);
    }

    /*
        Update the cinema
     */
    public void updateKino (Kino kino){
        proxy.getDao().updateKino(kino);
    }

    /*
        Delete the cinema by the owner
     */
    public void deleteKino (Kino kino){
        proxy.getDao().deleteKino(kino);
    }




}
