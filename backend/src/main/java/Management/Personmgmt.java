package Management;

import DAO.DAO_Proxy;
import POJO.Person;

import java.util.ArrayList;
import java.util.List;

public class Personmgmt {

    private static Personmgmt instance;
    DAO_Proxy proxy = DAO_Proxy.getInstance();
    private Person person;
    /*
       Create a Singleton Pattern Usermanagement
     */
    public static Personmgmt getInstance () {
        if (Personmgmt.instance == null) {
            Personmgmt.instance = new Personmgmt();
        }
        return Personmgmt.instance;
    }
    /*
        Constuctor of the class Usermanagement
     */
    public Personmgmt(){
        person=new Person();
    }

    public List getAllPerson(){
        return proxy.getDao().getallPersons();
    }


    /*
        Get the friends via the email
     */
    public Person searchFriendMail(String email){
        List <Person> personList=proxy.getDao().getallPersons();
        for(Person p : personList){
            if(p.getEmail().equals(email)){
                return p;
            }
        }
        return null;
    }

    public Person searchFriendName(String forename,String lastname){
        List <Person> personList=proxy.getDao().getallPersons();
        for(Person p : personList){
            if(p.getFirstname().equals(forename)&&p.getSurname().equals(lastname)){
                return p;
            }
        }
        return null;
    }

    public List<Person> getFriends(String email){

        List<Person> personList=proxy.getDao().getallPersons();
       List<Person> resultList=new ArrayList<Person>();
        for(Person p: personList)
        {
            if(p.getEmail().equals(email)){
                resultList=p.getFriends();
            }
        }
        return resultList;
    }

    public void addFriends(List<Person>personList){
        person.setFriends(personList);
    }
    public void addFriend(Person person){
        person.setFriend(person);
    }
}
