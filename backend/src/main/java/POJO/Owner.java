package POJO;



import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="OWNER")
public class Owner  implements Serializable {
    @Column (name = "IBAN")
    @Id
    private String iban;
    private String telephone;
    private String email;
    @ManyToMany (mappedBy = "owners")
    private List<Kino> possessions;
    @OneToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "EMAIL_FK")
    private Person person;

    public Owner(String email, String firstname, String surname, String country, String location, String zipcode, String street, String passwordhash, String iban, String telephone) {
        Person person = new Person (email, firstname,surname,country,location,zipcode,street, passwordhash);
        /*setFirstname(firstname);
        setSurname(surname);
        setCountry(country);
        setLocation(location);
        setZipcode(zipcode);
        setStreet(street); */

        this.telephone = telephone;
        this.email = email;
        this.iban = iban;
        this.setPerson(person);
        this.possessions = new ArrayList<>();
    }

    public Owner(String iban, String telephone, Person person) {
        this.iban = iban;
        this.telephone = telephone;
        this.person = person;
    }

    /**
     * default constructor for Hibernate
     */
    public Owner() {
        super();
    }


    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public List<Kino> getPossessions() {
        return possessions;
    }

    public void setPossessions(List<Kino> possessions) {
        this.possessions = possessions;
    }

    public void addKinotopossessions(Kino kino) {
        possessions.add(kino);
    }

    public void removeKinofrompossessions (Kino kino) {
        possessions.remove(kino);
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
