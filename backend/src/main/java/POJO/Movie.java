package POJO;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Entity
@Table (name = "MOVIE")
public class Movie implements Serializable {

    public enum Genre{
        Action, SciFi, Love, Fantasy, Thriller, Horror;

        public static Genre getRandomGenre() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }


    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "MOVIE_ID")
    private String id;
    private String title;
    @Column (name = "GENRE")
    private Genre genre;
    private int rating;
    @OneToMany(
            mappedBy = "movie",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    protected List<Film> filmList;

    public List<Film> getFilmList() {
        return filmList;
    }

    public void setFilmList(List<Film> filmList) {
        this.filmList = filmList;
    }

    public Movie () {
        this.filmList = new ArrayList<>();
    }

    public Movie(String title, Genre genre, int rating) {
        filmList = new ArrayList<Film>();
        this.title = title;
        this.genre = genre;
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void addFilm(Film film) {this.filmList.add(film);}
}
