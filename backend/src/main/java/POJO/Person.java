package POJO;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="PERSON")
public class Person implements Serializable {
    @Id
    @Column(name = "EMAIL", updatable = false, nullable = false)
    private String email;
    private String firstname;
    private String surname;
    private String country;
    private String location;
    private String zipcode;
    private String street;
    private String passwordhash;
    @Transient
    private String token;

    @OneToMany
    protected List<Person> friends;
    @OneToMany (
            mappedBy = "owner",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    protected List<Ticket> tickets;
    public Person(String email, String firstname, String surname, String country, String location, String zipcode, String street, String passwordhash) {
        this.firstname = firstname;
        this.surname = surname;
        this.country = country;
        this.location = location;
        this.zipcode = zipcode;
        this.street = street;
        this.email = email;
        this.passwordhash = passwordhash;
        tickets = new ArrayList<Ticket>();
        friends = new ArrayList<Person>();
    }

    //Constructor for Friends
    public Person (String email) {
        this.email = email;
    }

    public Person () {
        friends = new ArrayList<Person>();
        tickets = new ArrayList<Ticket>();

    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public List<Person> getFriends() {
        return friends;
    }

    public void setFriends(List<Person> friends) {
        this.friends = friends;
    }

    public void setFriend(Person person) {
        friends.add(person);
    }

    public void removeFriend(Person person) { //thats so sad
        friends.remove(person);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void addTicket(Ticket ticket) {
        tickets.add(ticket);
    }

    public void removeTicket(Ticket ticket) {
        tickets.remove(ticket);
    }

    public String getPasswordhash() {
        return passwordhash;
    }

    public void setPasswordhash(String passwordhash) {
        this.passwordhash = passwordhash;
    }

    public  void setToken(String token){this.token=token;}

    public String getToken(){return token;}

    public void addFriend(Person p){
        this.friends.add(p);
    }

}
