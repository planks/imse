package POJO;

import java.io.Serializable;

public class SaalPK implements Serializable{
    private int saalnumber;
    private Kino kinolocation;

    public SaalPK () {}

    public SaalPK(int saalnumber, Kino kinolocation) {
        this.saalnumber = saalnumber;
        this.kinolocation = kinolocation;
    }

    public int getSaalnumber() {
        return saalnumber;
    }

    public void setSaalnumber(int saalnumber) {
        this.saalnumber = saalnumber;
    }

    public Kino getKinolocation() {
        return kinolocation;
    }

    public void setKinolocation(Kino kinolocation) {
        this.kinolocation = kinolocation;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((kinolocation.getId() == null) ? 0 : kinolocation.getId().hashCode());
        result = prime * result + saalnumber;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SaalPK other = (SaalPK) obj;
        if (kinolocation.getId() == null) {
            if (other.kinolocation.getId() != null)
                return false;
        } else if (!getKinolocation().getId().equals(other.kinolocation.getId()))
            return false;
        if (saalnumber != other.saalnumber)
            return false;
        return true;
    }
}
