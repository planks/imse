package POJO;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="TICKET")
public class Ticket implements Serializable{

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String creditcardnumber;
    @Temporal(TemporalType.TIMESTAMP)
    private Date paymentdate;
    @ManyToOne
    @JoinColumn (name="FILM_ID")
    private Film film;
    @ManyToOne
    @JoinColumn (name="EMAIL_FK")
    private Person owner;

    public Ticket(){

    }

    public Ticket(String creditcardnumber, Date paymentdate, Film film, Person owner) {
        this.creditcardnumber = creditcardnumber;
        this.paymentdate = paymentdate;
        this.film = film;
        this.owner = owner;
    }

    public Ticket(String id, String creditcardnumber, Date paymentdate, Film film, Person owner) {
        this.id = id;
        this.creditcardnumber = creditcardnumber;
        this.paymentdate = paymentdate;
        this.film = film;
        this.owner = owner;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

   public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public String getCreditcardnumber() {
        return creditcardnumber;
    }

    public void setCreditcardnumber(String creditcardnumber) {
        this.creditcardnumber = creditcardnumber;
    }

    public Date getPaymentdate() {
        return paymentdate;
    }

    public void setPaymentdate(Date paymentdate) {
        this.paymentdate = paymentdate;
    }
}
