package POJO;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="STAFF")
public class Staff implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column (name = "STAFF_ID")
    private String id;
    private char sex;
    private double salary;
    @ManyToOne
    @JoinColumn (name = "KINO_ID")
    private Kino worklocation;
    @OneToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "EMAIL_FK")
    private Person person;

    public Staff(String email, String firstname, String surname, String country, String location, String zipcode, String street, String passwordhash, char sex, double salary, Kino worklocation) {
        Person person = new Person (email, firstname,surname,country,location,zipcode,street, passwordhash);
        this.id = id;
        this.sex = sex;
        this.salary = salary;
        this.person = person;
        this.worklocation = worklocation;
    }
    public Staff (char sex, double salary, Person person, Kino worklocation) {
        this.sex = sex;
        this.salary = salary;
        this.person = person;
        this.worklocation = worklocation;
    }


    /**
     * default constructor for Hibernate
     */
    public Staff() {}

    public Staff(String email, String firstname, String surname, String country, String location, String zipcode, String street, String passwordhash, char c, double v) {
        Person person = new Person (email, firstname,surname,country,location,zipcode,street, passwordhash);
        this.id = id;
        this.sex = sex;
        this.salary = salary;
        this.person = person;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Kino getWorklocation() {
        return worklocation;
    }

    public void setWorklocation(Kino worklocation) {
        this.worklocation = worklocation;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
