package POJO;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="KINO")
public class Kino implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column (name = "KINO_ID")
    private String id;
    private String name;
    private String street;
    private String location;
    private String zipcode;
    private String country;
    private String website;
    private String telephonenumber;
    @OneToMany(
            mappedBy = "worklocation",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    protected List<Staff> mitarbeiterlist;
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "KINO_OWNER",
            joinColumns = { @JoinColumn(name = "KINO_ID") },
            inverseJoinColumns = { @JoinColumn(name = "IBAN") }
    )
    protected List<Owner> owners;
    @OneToMany(
            mappedBy = "kinolocation",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    protected List<Saal> rooms;

    public Kino(String name, String street, String location, String zipcode, String country, String website, String telephonenumber) {
        this.name = name;
        this.street = street;
        this.location = location;
        this.zipcode = zipcode;
        this.country = country;
        this.website = website;
        this.telephonenumber = telephonenumber;
        this.mitarbeiterlist = new ArrayList<Staff>();
        this.owners = new ArrayList<Owner>();
        this.rooms = new ArrayList<Saal>();
    }

    public Kino () {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTelephonenumber() {
        return telephonenumber;
    }

    public void setTelephonenumber(String telephonenumber) {
        this.telephonenumber = telephonenumber;
    }

    public List<Staff> getMitarbeiterlist() {
        return mitarbeiterlist;
    }

    public void setMitarbeiterlist(List<Staff> mitarbeiterlist) {
        this.mitarbeiterlist = mitarbeiterlist;
    }

    public void addMitarbeiter (Staff staff) {
        mitarbeiterlist.add(staff);
    }

    public void removeMitarbeiter (Staff staff) {
        mitarbeiterlist.remove(staff);
    }

    public List<Owner> getOwners() {
        return owners;
    }

    public void setOwners(List<Owner> owners) {
        this.owners = owners;
    }

    public void addOwner (Owner owner) {
        owners.add(owner);
    }

    public void removeOwner (Owner owner) {
        owners.remove(owner);
    }

    public List<Saal> getRooms() {
        return rooms;
    }

    public void setRooms(List<Saal> rooms) {
        this.rooms = rooms;
    }

    public void addSaaltoRooms (Saal saal) {
        rooms.add(saal);
    }

    public void removeSaalfromRooms (Saal saal) {
        rooms.remove(saal);
    }
}
