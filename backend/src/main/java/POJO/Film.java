package POJO;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="FILM")
public class Film implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "FILM_ID")
    private String id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date beginn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;
    private int freeseats;
    @ManyToOne
    @JoinColumns(value =   {
            @JoinColumn(name = "SAAL_NUMBER_FK"),
            @JoinColumn(name = "KINO_ID_FK")
    })
    private Saal saal;
    @OneToMany (
            mappedBy = "film",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    protected List<Ticket> tickets;
    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn (name = "MOVIE_ID_FK")
    private Movie movie;

    public Film(Date beginn, Date end, Saal saal, Movie movie) {
        this.beginn = beginn;
        this.end = end;
        this.saal = saal;
        this.movie = movie;
        this.freeseats = saal.getNumberofseats();
        this.tickets = new ArrayList<>();
    }

    public Film(Date beginn, Date end, int seats, Saal saal, Movie movie) {
        this.beginn = beginn;
        this.end = end;
        this.saal = saal;
        this.movie = movie;
        this.freeseats = seats;
        this.tickets = new ArrayList<>();
    }
    public Film () {}

    public void setSaal(Saal saal) {
        this.saal = saal;
    }


    public Saal getSaal() {
        return saal;
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public void addTicket (Ticket ticket) throws Exception{
        tickets.add(ticket);
        this.freeseats -= 1;
    }

    public void removeTicket (Ticket ticket) {
        tickets.remove(ticket);
        this.freeseats += 1;
    }

    public Date getBeginn() {
        return beginn;
    }

    public void setBeginn(Date beginn) {
        this.beginn = beginn;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public int getFreeseats() {
        return freeseats;
    }

    public void setFreeseats(int freeseats) {
        this.freeseats = freeseats;
    }
}
