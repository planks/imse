package POJO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@IdClass(SaalPK.class)
@Table(name="SAAL")
public class Saal  {

    @Id
    @Column (name = "SAAL_NUMBER")
    private int saalnumber;
    @Id
    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn (name = "KINO_ID_FK")
    private Kino kinolocation;
    private int numberofseats;
    private String technoloy;

    @OneToMany(
            mappedBy = "saal",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    protected List<Film> filmList;

    public Saal () {}

    public Saal(int saalnumber, int numberofseats, String technoloy) {
        this.saalnumber = saalnumber;
        this.numberofseats = numberofseats;
        this.technoloy = technoloy;
        this.filmList = new ArrayList<>();
    }

    public int getSaalnumber() {
        return saalnumber;
    }

    public void setSaalnumber(int saalnumber) {
        this.saalnumber = saalnumber;
    }

    public int getNumberofseats() {
        return numberofseats;
    }

    public void setNumberofseats(int numberofseats) {
        this.numberofseats = numberofseats;
    }

    public String getTechnoloy() {
        return technoloy;
    }

    public void setTechnoloy(String technoloy) {
        this.technoloy = technoloy;
    }

    public List<Film> getFilmList() {
        return filmList;
    }

    public void setFilmList(List<Film> filmList) {
        this.filmList = filmList;
    }

    public Kino getKinolocation() {
        return kinolocation;
    }

    public void setKinolocation(Kino kinolocation) {
        this.kinolocation = kinolocation;
    }

    public void addFilm(Film f){ this.filmList.add(f);}

    public void removeFilm(Film f){this.filmList.remove(f);}

}

