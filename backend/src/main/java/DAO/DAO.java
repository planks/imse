package DAO;

import POJO.*;

import java.util.List;

public interface DAO {
    //GET
    public List<Film> getallFilms();

    List<Film> getallFilmsWithTickets();

    public List<Kino> getallKinos();

    List<Kino> getallKinosWithOwnersAndMitarbeiter();

    public List<Staff> getallStaff();
    public List<Owner> getallOwners();
    public List<Owner> getallOwnersWithPossessions();
    public List<Person> getallPersons();
    public List<Saal> getallSaal();
    public List<Ticket> getallTicket();
    public List<Movie> getallMovies ();
    public List<Movie> getallMoviesWithFilms ();

    public Film getFilmById(String id);

    public Person getPersonForBuyingTicketByPersonId(String id);

    public Film getFilmForBuyingTicketsByFilmId(String id);

    public Film getFilmWithSaalsAndTicketsForDeletingFilmByFilmId(String id);

    public List<Ticket> getTicketsForFilm(String filmId);
    public List<Movie> getMoviesByGenre(Movie.Genre genre);
    public List<Movie> getMoviesByRating(int rating);
    public Saal getSaalbyKinoandSaalnumber (String kinoid, int saalnumber);
    public Ticket getTicketByID(String id);
    public List<Ticket> getTicketsForPerson(String email);
    public Saal getSaalByID(String id);
    public List<Movie> getMoviebyTitle (String title);
    public Person getPersonbyID (String email);
    public Kino getKinoById(String kinoId);
    public Movie getMovieById(String movieId);
    public List<Staff> getStaffOfKinobyKinoID(String kinoId);
    public List<Owner> getOwnersOfKinobyKinoID(String kinoId);
    public List<Saal> getSaalsofKinobyKinoID (String kinoid);
    public Person getPersonwithFriendsbyEmail (String email);



    //CREATE
    public String createFilm(Film film);
    public String createPerson (Person person) throws Exception;
    public String createKino (Kino kino);
    public String createOwner (Owner owner) throws Exception;
    public String createSaal (Saal saal);
    public String createStaff (Staff staff) throws Exception;
    public String createTicket (Ticket ticket);
    public String createMovie (Movie movie);
    //DELETE
    public void deleteFilm (Film film);
    public void deleteKino (Kino kino);
    public void deleteOwner (Owner owner);
    public void deletePerson (Person person);
    public void deleteSaal (Saal saal);
    public void deleteStaff (Staff staff);
    public void deleteTicket (Ticket ticket);
    public void deleteMovie (Movie movie);
    //UPDATE
    public void updateFilm (Film film);
    public void updateKino (Kino kino);
    public void updateOwner (Owner owner);
    public void updatePerson (Person person) throws Exception;
    public void updateSaal (Saal saal);
    public void updateStaff (Staff staff);
    public void updateTicket (Ticket ticket);
    public void updateMovie (Movie movie) throws Exception;

    Movie getMovieWithFilmTicketsById(String movieId);

    void dropAll();
}
