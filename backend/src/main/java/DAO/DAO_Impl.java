package DAO;

import DataFiller.DataFiler;
import POJO.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.List;

@WebListener
public class DAO_Impl implements DAO, ServletContextListener{

    private static DAO_Impl instance;
    static SessionFactory sessionFactory;
    static Configuration configuration;

    /**
     * Classical Singleton implementation
     */
    public static DAO_Impl getInstance () {
        if (DAO_Impl.instance == null) {
            DAO_Impl.instance = new DAO_Impl();
        }
        return DAO_Impl.instance;
    }

    public DAO_Impl(){
        configuration = new Configuration();

    }

    public void contextInitialized(ServletContextEvent event) {
        DAO_Impl.instance = new DAO_Impl();
        initializeDBconnection();
    }

    public void contextDestroyed(ServletContextEvent event) {
        // Webapp shutdown.
    }


    private void initializeDBconnection () {
        configuration.addAnnotatedClass(Owner.class);
        configuration.addAnnotatedClass(Film.class);
        configuration.addAnnotatedClass(Kino.class);
        configuration.addAnnotatedClass(Staff.class);
        configuration.addAnnotatedClass(Person.class);
        configuration.addAnnotatedClass(Ticket.class);
        configuration.addAnnotatedClass(Saal.class);
        configuration.addAnnotatedClass(Movie.class);
        sessionFactory = configuration.configure().buildSessionFactory();
        DataFiler.initialFilling();
    }

    public List<Film> getallFilms() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Film> criteriaQuery = session.getCriteriaBuilder().createQuery(Film.class);
        Root<Film> filmRoot = criteriaQuery.from(Film.class);
        criteriaQuery.select(filmRoot);
        List<Film> films = session.createQuery(criteriaQuery).getResultList();
        for (Film film: films) {
            film.getSaal();
            film.getMovie();
        }
        session.close();
        return films;
    }
    @Override
    public List<Film> getallFilmsWithTickets() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Film> criteriaQuery = session.getCriteriaBuilder().createQuery(Film.class);
        Root<Film> filmRoot = criteriaQuery.from(Film.class);
        criteriaQuery.select(filmRoot);
        List<Film> films = session.createQuery(criteriaQuery).getResultList();
        for (Film film: films) {
            film.getSaal();
            film.getMovie();
            film.getTickets().size();
        }
        session.close();
        return films;
    }

    public List<Kino> getallKinos() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Kino> criteriaQuery = session.getCriteriaBuilder().createQuery(Kino.class);
        Root<Kino> kinoRoot = criteriaQuery.from(Kino.class);
        criteriaQuery.select(kinoRoot);
        List<Kino> kinos = session.createQuery(criteriaQuery).getResultList();
        for (Kino kino:kinos) {
            kino.getRooms().size();
        }
        session.close();
        return kinos;
    }

    @Override
    public List<Kino> getallKinosWithOwnersAndMitarbeiter() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Kino> criteriaQuery = session.getCriteriaBuilder().createQuery(Kino.class);
        Root<Kino> kinoRoot = criteriaQuery.from(Kino.class);
        criteriaQuery.select(kinoRoot);
        List<Kino> kinos = session.createQuery(criteriaQuery).getResultList();
        for (Kino kino:kinos) {
            kino.getOwners().size();
            kino.getMitarbeiterlist().size();
            kino.getRooms().size();
        }
        session.close();
        return kinos;
    }

    public List<Staff> getallStaff() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Staff> criteriaQuery = session.getCriteriaBuilder().createQuery(Staff.class);
        Root<Staff> staffRoot = criteriaQuery.from(Staff.class);
        criteriaQuery.select(staffRoot);
        List<Staff> staffList = session.createQuery(criteriaQuery).getResultList();
        session.close();
        return staffList;
    }

    public List<Owner> getallOwners() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Owner> criteriaQuery = session.getCriteriaBuilder().createQuery(Owner.class);
        Root<Owner> ownerRoot = criteriaQuery.from(Owner.class);
        criteriaQuery.select(ownerRoot);
        List<Owner> ownerList = session.createQuery(criteriaQuery).getResultList();
        session.close();
        return ownerList;
    }

    public List<Owner> getallOwnersWithPossessions() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Owner> criteriaQuery = session.getCriteriaBuilder().createQuery(Owner.class);
        Root<Owner> ownerRoot = criteriaQuery.from(Owner.class);
        criteriaQuery.select(ownerRoot);
        List<Owner> ownerList = session.createQuery(criteriaQuery).getResultList();
        for (Owner o : ownerList){
            o.getPossessions().size();
        }
        session.close();
        return ownerList;
    }

    public List<Person> getallPersons() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Person> criteriaQuery = session.getCriteriaBuilder().createQuery(Person.class);
        Root<Person> personRoot = criteriaQuery.from(Person.class);
        criteriaQuery.select(personRoot);
        List<Person> personList = session.createQuery(criteriaQuery).getResultList();
        for (Person person : personList) {
            person.getFriends().size();
            person.getTickets().size();
        }
        session.close();
        return personList;
    }

    public List<Saal> getallSaal() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Saal> criteriaQuery = session.getCriteriaBuilder().createQuery(Saal.class);
        Root<Saal> saalRoot = criteriaQuery.from(Saal.class);
        criteriaQuery.select(saalRoot);
        List<Saal> saalList = session.createQuery(criteriaQuery).getResultList();
        for (Saal saal : saalList){
            saal.getFilmList().size();
        }
        session.close();
        return saalList;
    }

    public List<Ticket> getallTicket() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Ticket> criteriaQuery = session.getCriteriaBuilder().createQuery(Ticket.class);
        Root<Ticket> ticketRoot = criteriaQuery.from(Ticket.class);
        criteriaQuery.select(ticketRoot);
        List<Ticket> ticketList = session.createQuery(criteriaQuery).getResultList();
        session.close();
        return ticketList;
    }

    public List<Movie> getallMovies () {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Movie> criteriaQuery = session.getCriteriaBuilder().createQuery(Movie.class);
        Root<Movie> movieRoot = criteriaQuery.from(Movie.class);
        criteriaQuery.select(movieRoot);
        List<Movie> movieList = session.createQuery(criteriaQuery).getResultList();
        session.close();
        return movieList;

    }

    public List<Movie> getallMoviesWithFilms () {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaQuery<Movie> criteriaQuery = session.getCriteriaBuilder().createQuery(Movie.class);
        Root<Movie> movieRoot = criteriaQuery.from(Movie.class);
        criteriaQuery.select(movieRoot);
        List<Movie> movieList = session.createQuery(criteriaQuery).getResultList();
        for (Movie m : movieList) {
            m.getFilmList().size();
        }
        session.close();
        return movieList;

    }

    public Film getFilmById(String id) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Film film = (Film) session.get(Film.class,id);
        film.getSaal();
        film.getMovie();
        session.getTransaction().commit();
        session.close();
        return  film;
    }

    @Override
    public Person getPersonForBuyingTicketByPersonId(String personId) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Person person = session.get(Person.class,personId);
        person.getTickets().size();
        session.getTransaction().commit();
        session.close();
        return person;
    }

    @Override
    public Film getFilmForBuyingTicketsByFilmId(String id) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Film film = (Film) session.get(Film.class,id);
        film.getSaal();
        film.getMovie();
        film.getTickets().size();
        session.getTransaction().commit();
        session.close();
        return film;
    }

    @Override
    public Film getFilmWithSaalsAndTicketsForDeletingFilmByFilmId(String filmId) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Film film = (Film) session.get(Film.class,filmId);
        film.getSaal().getFilmList().size();
        film.getSaal().getKinolocation().toString();
        film.getTickets().size();
        session.getTransaction().commit();
        session.close();
        return film;
    }

    @Override
    public Movie getMovieWithFilmTicketsById(String movieId) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Movie movie = (Movie) session.get(Movie.class,movieId);
        movie.getFilmList().size();
        for (Film f : movie.getFilmList()){
            f.getTickets().size();
            f.getMovie();
            f.getSaal().getKinolocation().getId();
        }
        session.getTransaction().commit();
        session.close();
        return movie;
    }

    @Override
    public void dropAll() {

    }

    @Override
    public List<Ticket> getTicketsForFilm(String filmId) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Film film = (Film) session.get(Film.class,filmId);
        List<Ticket> ticketList = film.getTickets();
        ticketList.size();
        session.getTransaction().commit();
        session.close();
        return ticketList;
    }

    public List<Movie> getMoviesByGenre(Movie.Genre genre) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Movie> criteriaQuery = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> movieRoot = criteriaQuery.from(Movie.class);
        EntityType<Movie> MOV_ = session.getMetamodel().entity(Movie.class);
        criteriaQuery.select(movieRoot).where(criteriaBuilder.equal(movieRoot.get("genre"),genre));
        List<Movie> movieList = session.createQuery(criteriaQuery).getResultList();
        /*for (Movie movie:movieList) {
            List<Film> filmList = movie.getFilmList();
            for (Film film:filmList) {
                film.getSaal();
            }
        }*/
        session.close();
        return movieList;
    }

    public List<Movie> getMoviesByRating(int rating) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Movie> criteriaQuery = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> movieRoot = criteriaQuery.from(Movie.class);
        EntityType<Movie> MOV_ = session.getMetamodel().entity(Movie.class);
        criteriaQuery.select(movieRoot).where(criteriaBuilder.equal(movieRoot.get("rating"),rating));
        List<Movie> movieList = session.createQuery(criteriaQuery).getResultList();
       /* for (Movie movie:movieList) {
            List<Film> filmList = movie.getFilmList();
            for (Film film:filmList) {
                film.getSaal();
            }
        }*/
        session.close();
        return movieList;
    }

    public List<Movie> getMoviebyTitle (String title) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Movie> criteriaQuery = criteriaBuilder.createQuery(Movie.class);
        Root<Movie> movieRoot = criteriaQuery.from(Movie.class);
        EntityType<Movie> MOV_ = session.getMetamodel().entity(Movie.class);
        criteriaQuery.select(movieRoot).where(criteriaBuilder.equal(movieRoot.get("title"),title));
        List<Movie> movieList = session.createQuery(criteriaQuery).getResultList();
        /*for (Movie movie:movieList) {
            List<Film> films = movie.getFilmList();
            for (Film film:films) {
                film.getSaal().getSaalnumber();
            }
        }*/
        session.close();
        return movieList;
    }

    public Saal getSaalbyKinoandSaalnumber(String kinoid, int saalnumber) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Kino kino = (Kino) session.get(Kino.class,kinoid);
        List<Saal> saalList = kino.getRooms();
        Saal saal = null;
        for (Saal s : saalList) {
            if (s.getSaalnumber() == saalnumber){
                saal = s;
                saal.getFilmList().size();
            }
        }
        session.getTransaction().commit();
        session.close();
        return saal;
    }

    @Override
    public Ticket getTicketByID(String id) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Ticket ticket = (Ticket) session.get(Ticket.class,id);
        Film film = ticket.getFilm();
        session.getTransaction().commit();
        session.close();
        return  ticket;
    }
    @Override
    public List<Ticket> getTicketsForPerson(String email) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Person person = (Person) session.get(Person.class,email);
        List<Ticket> tickets = person.getTickets();
        for (Ticket ticket:tickets) {
            ticket.getFilm().getMovie();
            ticket.getFilm().getSaal();
        }
        session.getTransaction().commit();
        session.close();
        return tickets;
    }

    @Override
    public Saal getSaalByID(String id) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Saal saal = (Saal) session.get(Saal.class,id);
        session.getTransaction().commit();
        session.close();
        return  saal;
    }

    @Override
    public Person getPersonbyID(String email) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Person person = (Person) session.get(Person.class,email);
        session.getTransaction().commit();
        session.close();
        return  person;
    }

    @Override
    public Kino getKinoById(String id) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Kino kino = (Kino) session.get(Kino.class,id);
        List<Saal> saals = kino.getRooms();
        for (Saal saal:saals) {
            saal.getFilmList().size();
        }
        session.getTransaction().commit();
        session.close();
        return  kino;
    }

    @Override
    public Movie getMovieById(String id) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Movie movie = (Movie) session.get(Movie.class,id);
        List<Film> films = movie.getFilmList();
        for (Film film:films) {
            film.getSaal().getSaalnumber();
        }
        session.getTransaction().commit();
        session.close();
        return  movie;
    }

    @Override
    public List<Staff> getStaffOfKinobyKinoID(String kinoId) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Kino kino = (Kino) session.get(Kino.class,kinoId);
        List<Staff> staffList = kino.getMitarbeiterlist();
        for (Staff staff : staffList) {
            staff.getPerson().getEmail();
        }
        session.getTransaction().commit();
        session.close();
        return  staffList;
    }


    @Override
    public List<Owner> getOwnersOfKinobyKinoID(String kinoId) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Kino kino = (Kino) session.get(Kino.class,kinoId);
        List<Owner> ownerList = kino.getOwners();
        for (Owner owner : ownerList) {
            owner.getPerson().getEmail();
        }
        session.getTransaction().commit();
        session.close();
        return  ownerList;
    }

    @Override
    public List<Saal> getSaalsofKinobyKinoID(String kinoid) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Kino kino = (Kino) session.get(Kino.class,kinoid);
        List<Saal> saals = kino.getRooms();
        for (Saal saal:saals) {
            saal.getFilmList().size();
        }
        session.getTransaction().commit();
        session.close();
        return  saals;
    }

    @Override
    public Person getPersonwithFriendsbyEmail(String email) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        Person person = (Person) session.get(Person.class,email);
        person.getFriends().size();
        session.getTransaction().commit();
        session.close();
        return  person;
    }

    public String createFilm(Film film) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(film);
        session.getTransaction().commit();
        session.close();
        return film.getId();
    }

    public String createPerson(Person person) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(person);
        session.getTransaction().commit();
        session.close();
        return person.getEmail();
    }

    public String createKino(Kino kino) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(kino);
        session.getTransaction().commit();
        session.close();
        return kino.getId();
    }

    public String createOwner(Owner owner) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(owner);
        session.getTransaction().commit();
        session.close();
        return owner.getIban();
    }

    public String createSaal(Saal saal) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(saal);
        session.getTransaction().commit();
        session.close();
        return ""+saal.getSaalnumber();
    }

    public String createStaff(Staff staff) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(staff);
        session.getTransaction().commit();
        session.close();
        return staff.getId();
    }

    public String createTicket(Ticket ticket) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(ticket);
        session.getTransaction().commit();
        session.close();
        return ticket.getId();
    }

    public String createMovie(Movie movie) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(movie);
        session.getTransaction().commit();
        session.close();
        return movie.getId();

    }

    public void deleteFilm(Film film) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.delete(film);
        session.getTransaction().commit();
        session.close();

    }

    public void deleteKino(Kino kino) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.delete(kino);
        session.getTransaction().commit();
        session.close();

    }

    public void deleteOwner(Owner owner) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.delete(owner);
        session.getTransaction().commit();
        session.close();

    }

    public void deletePerson(Person person) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.delete(person);
        session.getTransaction().commit();
        session.close();

    }

    public void deleteSaal(Saal saal) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.delete(saal);
        session.getTransaction().commit();
        session.close();

    }

    public void deleteStaff(Staff staff) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.delete(staff);
        session.getTransaction().commit();
        session.close();

    }

    public void deleteTicket(Ticket ticket) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.delete(ticket);
        session.getTransaction().commit();
        session.close();
    }

    public void deleteMovie (Movie movie) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.delete(movie);
        session.getTransaction().commit();
        session.close();
    }

    public void updateFilm(Film film) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.update(film);
        session.getTransaction().commit();
        session.close();

    }

    public void updateKino(Kino kino) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.update(kino);
        session.getTransaction().commit();
        session.close();

    }

    public void updateOwner(Owner owner) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.update(owner);
        session.getTransaction().commit();
        session.close();

    }

    public void updatePerson(Person person) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.update(person);
        session.getTransaction().commit();
        session.close();

    }

    public void updateSaal(Saal saal) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.update(saal);
        session.getTransaction().commit();
        session.close();

    }

    public void updateStaff(Staff staff) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.update(staff);
        session.getTransaction().commit();
        session.close();

    }

    public void updateTicket(Ticket ticket) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.update(ticket);
        session.getTransaction().commit();
        session.close();
    }

    public void updateMovie (Movie movie) {
        Session session=sessionFactory.openSession();
        session.beginTransaction();
        session.update(movie);
        session.getTransaction().commit();
        session.close();

    }



}
