package DAO;

import POJO.*;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class DAO_Impl_Mongo implements DAO {
    private MongoCollection<Document> personCollection;
    private MongoCollection<Document> cinemaCollection;
    private MongoCollection<Document> movieCollection;


    public DAO_Impl_Mongo() {
        MongoClientURI uri = new MongoClientURI("mongodb://imse2018:cineplexx1234@ds155164.mlab.com:55164/cineplexx");
        MongoClient client = new MongoClient(uri);
        MongoDatabase db = client.getDatabase(uri.getDatabase());
        personCollection = db.getCollection("person");
        cinemaCollection = db.getCollection("cinema");
        movieCollection = db.getCollection("movies");

    }

    @Override
    public List<Film> getallFilms() {
        final List<Film> filmList = new ArrayList<Film>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {

                Movie movie = new Movie(
                        document.getString("title"),
                        Movie.Genre.valueOf(document.getString("genre")),
                        document.getInteger("rating"));
                if (document.get("film") != null) {
                    ArrayList<Document> filmDocumentList = (ArrayList<Document>) document.get("film");
                    for (Document filmDocument : filmDocumentList) {
                        Kino kino = new Kino();
                        kino.setName(filmDocument.getString("kinoname"));

                        Saal saal = new Saal();
                        saal.setSaalnumber(filmDocument.getInteger("saalnumber"));
                        saal.setKinolocation(kino);

                        Film film=new Film(
                                filmDocument.getDate("starttime"),
                                filmDocument.getDate("endtime"),
                                saal,movie);
                        film.setId(filmDocument.getInteger("filmid")+"");
                        film.setMovie(movie);
                        filmList.add(film);
                    }
                }
            }
        };
        movieCollection.find().forEach(documentMapper);
        return filmList;
    }
    //not needed
    @Override
    public List<Film> getallFilmsWithTickets() {
        return null;
    }

    @Override
    public List<Kino> getallKinos() {
        final List<Kino> cinemaList = new ArrayList<Kino>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                System.out.println(document.toJson());
                Kino kino = new Kino(
                        document.getString("kinoname"),
                        document.getString("street"),
                        document.getString("location"),
                        document.getString("zipcode"),
                        document.getString("country"),
                        document.getString("website"),
                        document.getString("telephonenumber"));
                kino.setId(document.getInteger("kinoid")+"");
                if (document.get("saal") != null) {
                    ArrayList<Document> saaldocumentlist = (ArrayList<Document>) document.get("saal");
                    for (Document saalDocument : saaldocumentlist) {
                        Saal newsaal = new Saal(
                                saalDocument.getInteger("saalnumber"),
                                saalDocument.getInteger("numberofseats"),
                                saalDocument.getString("technology"));
                        kino.addSaaltoRooms(newsaal);

                    }
                }

                cinemaList.add(kino);
            }
        };
        cinemaCollection.find().forEach(documentMapper);
        return cinemaList;

    }

    //not needed
    @Override
    public List<Kino> getallKinosWithOwnersAndMitarbeiter() {
        return null;
    }

    //not needed
    @Override
    public List<Staff> getallStaff() {
        return null;
    }

    //not needed
    @Override
    public List<Owner> getallOwners() {
        return null;
    }

    //not needed
    @Override
    public List<Owner> getallOwnersWithPossessions() {
        return null;
    }

    @Override
    public List<Person> getallPersons() {
        final List<Person> personList = new ArrayList<Person>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                System.out.println(document.toJson());
                Person person = new Person(
                        document.getString("email"),
                        document.getString("firstname"),
                        document.getString("surname"),
                        document.getString("country"),
                        document.getString("location"),
                        document.getString("zipcode"),
                        document.getString("street"),
                        document.getString("passwordhash"));
                if (document.get("friends") != null) {
                    ArrayList<Document> friendsdocumentlist = (ArrayList<Document>) document.get("friends");
                    for (Document friendsdocument : friendsdocumentlist) {
                        person.setFriend(new Person(friendsdocument.getString("friends_email")));
                    }
                }
                if (document.get("tickets") != null) {
                    ArrayList<Document> ticketsdocumentlist = (ArrayList<Document>) document.get("tickets");
                    for (Document ticketsdocument : ticketsdocumentlist) {
                        Kino kino = new Kino();
                        kino.setName(ticketsdocument.getString("kinoname"));
                        Film film = new Film();
                        film.setBeginn(ticketsdocument.getDate("starttime"));
                        Movie movie = new Movie();
                        movie.setTitle(ticketsdocument.getString("filmname"));
                        Saal saal = new Saal();
                        saal.setSaalnumber(ticketsdocument.getInteger("saalnumber"));
                        film.setMovie(movie);
                        film.setSaal(saal);
                        saal.setKinolocation(kino);
                        Ticket ticket = new Ticket(
                                Integer.toString(ticketsdocument.getInteger("ticket_id")),
                                ticketsdocument.getString("creditcardnumber"),
                                ticketsdocument.getDate("paymentdate"),
                                film,
                                person);
                        person.addTicket(ticket);

                    }
                }

                personList.add(person);
            }
        };
        personCollection.find().forEach(documentMapper);
        return personList;
    }

    @Override
    public List<Saal> getallSaal() {
        final List<Saal> saalList = new ArrayList<Saal>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                if (document.get("saal") != null) {
                    ArrayList<Document> saaldocumentlist = (ArrayList<Document>) document.get("saal");
                    for (Document saaldocument : saaldocumentlist) {
                        saalList.add(new Saal(
                                saaldocument.getInteger("saalnumber"),
                                saaldocument.getInteger("numberofseats"),
                                saaldocument.getString("technology")));
                    }
                }

            }

        };
        return saalList;


    }

    @Override
    public List<Ticket> getallTicket() {
        final List<Ticket> ticketList = new ArrayList<Ticket>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                if (document.get("tickets") != null) {
                    ArrayList<Document> ticketsdocumentlist = (ArrayList<Document>) document.get("tickets");
                    for (Document ticketsdocument : ticketsdocumentlist) {
                        Kino kino = new Kino();
                        kino.setName(ticketsdocument.getString("kinoname"));
                        Film film = new Film();
                        film.setBeginn(ticketsdocument.getDate("starttime"));
                        Movie movie = new Movie();
                        movie.setTitle(ticketsdocument.getString("filmname"));
                        Saal saal = new Saal();
                        saal.setSaalnumber(ticketsdocument.getInteger("saalnumber"));
                        film.setMovie(movie);
                        film.setSaal(saal);
                        saal.setKinolocation(kino);
                        Ticket ticket = new Ticket(
                                ticketsdocument.getString("creditcardnumber"),
                                ticketsdocument.getDate("paymentdate"),
                                film,
                                getPersonbyID(ticketsdocument.getString("email")));
                        ticketList.add(ticket);
                    }
                }
            }

        };
        personCollection.find().forEach(documentMapper);

        return ticketList;
    }

    @Override
    public List<Movie> getallMovies() {
        final List<Movie> filmList = new ArrayList<Movie>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                Movie movie = new Movie(
                        document.getString("title"),
                        Movie.Genre.valueOf(document.getString("genre")),
                        document.getInteger("rating")
                        );
                movie.setId(document.getInteger("movieid")+"");
                if (document.get("film") != null) {
                    ArrayList<Document> filmDocumentList = (ArrayList<Document>) document.get("film");
                    for (Document filmDocument : filmDocumentList) {
                        Kino kino = new Kino();
                        kino.setName(filmDocument.getString("kinoname"));


                        //movie.setTitle(filmDocument.getString("filmname"));
                        Saal saal = new Saal();
                        saal.setSaalnumber(filmDocument.getInteger("saalnumber"));
                        saal.setKinolocation(kino);

                        Film film = new Film(
                                filmDocument.getDate("starttime"),
                                filmDocument.getDate("endtime"),
                                saal, movie);
                        film.setId(document.getInteger("filmid")+"");
                        film.setSaal(saal);
                        movie.addFilm(film);

                    }
                }
                filmList.add(movie);
            }
        };
        movieCollection.find().forEach(documentMapper);
        return filmList;
    }

    //not needed
    @Override
    public List<Movie> getallMoviesWithFilms() {
        return getallMovies();
    }

    @Override
    public Film getFilmById(String id) {
        List<Film> filmList = getallFilms();
        for (Film f : filmList){
            if (f.getId().equals(id)){
                return f;
            }
        }
        return null;
    }

    @Override
    public Person getPersonForBuyingTicketByPersonId(String id) {
        return getPersonbyID(id);
    }

    @Override
    public Film getFilmForBuyingTicketsByFilmId(String id) {
        return getFilmWithSaalsAndTicketsForDeletingFilmByFilmId(id);
    }


    @Override
    public Film getFilmWithSaalsAndTicketsForDeletingFilmByFilmId(String id) {
        List<Film> filmList = getallFilms();
        Film film = null;
        for (Film f : filmList){
            if (f.getId().equals(id))
                film = f;
        }
        List<Ticket> ticketList = getTicketsForFilm(id);
        film.setTickets(ticketList);
        return film;
    }

    @Override
    public List<Ticket> getTicketsForFilm(final String filmId) {
        final List<Ticket> ticketList = new ArrayList<>();
        Block<Document> documentmapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                if (document.get("tickets") != null) {
                    ArrayList<Document> ticketslist = (ArrayList<Document>) document.get("tickets");
                    for (Document ticketdocument : ticketslist) {
                        if ((ticketdocument.getString("film_id")).equals(filmId)) {
                            Kino kino = new Kino();
                            kino.setName(ticketdocument.getString("kinoname"));
                            Film film = new Film();
                            film.setBeginn(ticketdocument.getDate("starttime"));
                            Movie movie = new Movie();
                            movie.setTitle(ticketdocument.getString("filmname"));
                            Saal saal = new Saal();
                            saal.setSaalnumber(ticketdocument.getInteger("saalnumber"));
                            film.setMovie(movie);
                            film.setSaal(saal);
                            saal.setKinolocation(kino);
                            Ticket ticket = new Ticket(
                                    Integer.toString(ticketdocument.getInteger("ticket_id")),
                                    ticketdocument.getString("creditcardnumber"),
                                    ticketdocument.getDate("paymentdate"),
                                    film,
                                    null);
                            ticketList.add(ticket);
                        }
                    }
                }
            }
        };
        personCollection.find().forEach(documentmapper);

        return ticketList;
    }

    @Override
    public List<Movie> getMoviesByGenre(final Movie.Genre genre) {
        //Document document = movieCollection.find(eq("genre", genre.toString())).first();
        final List<Movie> filmList = new ArrayList<Movie>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                Movie movie = new Movie(
                        document.getString("title"),
                        Movie.Genre.valueOf(document.getString("genre")),
                        document.getInteger("rating")
                );
                if (movie.getGenre() == genre) {
                    if (document.get("film") != null) {
                        ArrayList<Document> filmDocumentList = (ArrayList<Document>) document.get("film");
                        for (Document filmDocument : filmDocumentList) {
                            Kino kino = new Kino();
                            kino.setName(filmDocument.getString("kinoname"));

                            Saal saal = new Saal();
                            saal.setSaalnumber(filmDocument.getInteger("saalnumber"));
                            saal.setKinolocation(kino);


                            Film film = new Film(
                                    filmDocument.getDate("starttime"),
                                    filmDocument.getDate("endtime"),
                                    filmDocument.getInteger("freeseats"),
                                    saal, movie);
                            film.setSaal(saal);
                            movie.addFilm(film);
                        }
                    }
                    filmList.add(movie);
                }
            }
        };
        movieCollection.find().forEach(documentMapper);
        return filmList;
    }

    @Override
    public List<Movie> getMoviesByRating(final int rating) {
        final List<Movie> filmList = new ArrayList<Movie>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                Movie movie = new Movie(
                        document.getString("title"),
                        Movie.Genre.valueOf(document.getString("genre")),
                        document.getInteger("rating")
                );
                if (movie.getRating() == rating) {
                    if (document.get("film") != null) {
                        ArrayList<Document> filmDocumentList = (ArrayList<Document>) document.get("film");
                        for (Document filmDocument : filmDocumentList) {
                            Kino kino = new Kino();
                            kino.setName(filmDocument.getString("kinoname"));

                            Saal saal = new Saal();
                            saal.setSaalnumber(filmDocument.getInteger("saalnumber"));
                            saal.setKinolocation(kino);


                            Film film = new Film(
                                    filmDocument.getDate("starttime"),
                                    filmDocument.getDate("endtime"),
                                    filmDocument.getInteger("freeseats"),
                                    saal, movie);
                            film.setSaal(saal);
                            movie.addFilm(film);
                        }
                    }
                    filmList.add(movie);
                }
            }
        };
        movieCollection.find().forEach(documentMapper);
        return filmList;
    }

    @Override
    public Saal getSaalbyKinoandSaalnumber(String kinoid, int saalnumber) {
        return null;
    }

    @Override
    public Ticket getTicketByID(final String id) {
        final Document[] ticketdocumentfinal = new Document[1];
        final Person[] person = new Person[1];
        Block<Document> documentmapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                if (document.get("tickets") != null) {
                    ArrayList<Document> ticketslist = (ArrayList<Document>) document.get("tickets");
                    for (Document ticketdocument : ticketslist) {
                        if (Integer.toString(ticketdocument.getInteger("ticket_id")).equals(id)) {
                            ticketdocumentfinal[0] = ticketdocument;
                        }
                    }
                    String personid = document.getString("email");
                    person[0] = getPersonbyID(personid);
                }
            }
        };
        personCollection.find().forEach(documentmapper);
        if (ticketdocumentfinal[0]!=null) {


            Kino kino = new Kino();
            kino.setName(ticketdocumentfinal[0].getString("kinoname"));
            Film film = new Film();
            film.setBeginn(ticketdocumentfinal[0].getDate("starttime"));
            film.setId(ticketdocumentfinal[0].getString("film_id"));
            Movie movie = new Movie();
            movie.setTitle(ticketdocumentfinal[0].getString("filmname"));
            Saal saal = new Saal();
            saal.setSaalnumber(ticketdocumentfinal[0].getInteger("saalnumber"));
            film.setMovie(movie);
            film.setSaal(saal);
            saal.setKinolocation(kino);
            Ticket ticket = new Ticket(
                    Integer.toString(ticketdocumentfinal[0].getInteger("ticket_id")),
                    ticketdocumentfinal[0].getString("creditcardnumber"),
                    ticketdocumentfinal[0].getDate("paymentdate"),
                    film,
                    person[0]);
            return ticket;
        } return null;
    }

    @Override
    public List<Ticket> getTicketsForPerson(String email) {
        Document document = personCollection.find(eq("email", email)).first();
        List<Ticket> ticketList = new ArrayList<Ticket>();
        if (document.get("tickets") != null) {
            ArrayList<Document> ticketsdocumentlist = (ArrayList<Document>) document.get("tickets");
            for (Document ticketsdocument : ticketsdocumentlist) {
                Kino kino = new Kino();
                kino.setName(ticketsdocument.getString("kinoname"));
                Film film = new Film();
                film.setBeginn(ticketsdocument.getDate("starttime"));
                Movie movie = new Movie();
                movie.setTitle(ticketsdocument.getString("filmname"));
                Saal saal = new Saal();
                saal.setSaalnumber(ticketsdocument.getInteger("saalnumber"));
                film.setMovie(movie);
                film.setSaal(saal);
                saal.setKinolocation(kino);
                Ticket ticket = new Ticket(
                        ticketsdocument.getString("creditcardnumber"),
                        ticketsdocument.getDate("paymentdate"),
                        film,
                        null);
                ticket.setId(Integer.toString(ticketsdocument.getInteger("ticket_id")));
                ticketList.add(ticket);
            }
        }
        return ticketList;
    }

    @Override
    public Saal getSaalByID(String id) {
        Document document = cinemaCollection.find(eq("kinoid", id)).first();
        return new Saal(
                document.getInteger("saalnumber"),
                document.getInteger("numberofseats"),
                document.getString("technology")
        );
    }

    @Override
    public List<Movie> getMoviebyTitle(final String title) {
        Document document = movieCollection.find(eq("title", title)).first();
        final List<Movie> filmList = new ArrayList<Movie>();
        Block<Document> documentMapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                Movie movie = new Movie(
                        document.getString("title"),
                        Movie.Genre.valueOf(document.getString("genre")),
                        document.getInteger("rating")
                );
                if (movie.getTitle().equals(title)) {
                    if (document.get("film") != null) {
                        ArrayList<Document> filmDocumentList = (ArrayList<Document>) document.get("film");
                        for (Document filmDocument : filmDocumentList) {
                            Kino kino = new Kino();
                            kino.setName(filmDocument.getString("kinoname"));

                            Saal saal = new Saal();
                            saal.setSaalnumber(filmDocument.getInteger("saalnumber"));
                            saal.setKinolocation(kino);


                            Film film = new Film(
                                    filmDocument.getDate("starttime"),
                                    filmDocument.getDate("endtime"),
                                    filmDocument.getInteger("freeseats"),
                                    saal, movie);
                            film.setSaal(saal);
                            movie.addFilm(film);
                        }
                    }
                    filmList.add(movie);
                }
            }
        };
        movieCollection.find().forEach(documentMapper);
        return filmList;
    }

    @Override
    public Person getPersonbyID(String email) {
        Document document = personCollection.find(eq("email", email)).first();
        Person person = new Person(
                document.getString("email"),
                document.getString("firstname"),
                document.getString("surname"),
                document.getString("country"),
                document.getString("location"),
                document.getString("zipcode"),
                document.getString("street"),
                document.getString("passwordhash"));
        if (document.get("friends") != null) {
            ArrayList<Document> friendsdocumentlist = (ArrayList<Document>) document.get("friends");
            for (Document friendsdocument : friendsdocumentlist) {
                person.setFriend(new Person(friendsdocument.getString("friends_email")));
            }
        }
        if (document.get("tickets") != null) {
            ArrayList<Document> ticketsdocumentlist = (ArrayList<Document>) document.get("tickets");
            for (Document ticketsdocument : ticketsdocumentlist) {
                Kino kino = new Kino();
                kino.setName(ticketsdocument.getString("kinoname"));
                Film film = new Film();
                film.setBeginn(ticketsdocument.getDate("starttime"));
                Movie movie = new Movie();
                movie.setTitle(ticketsdocument.getString("filmname"));
                Saal saal = new Saal();
                saal.setSaalnumber(ticketsdocument.getInteger("saalnumber"));
                film.setMovie(movie);
                film.setSaal(saal);
                saal.setKinolocation(kino);
                Ticket ticket = new Ticket(
                        Integer.toString(ticketsdocument.getInteger("ticket_id")),
                        ticketsdocument.getString("creditcardnumber"),
                        ticketsdocument.getDate("paymentdate"),
                        film,
                        person);
                person.addTicket(ticket);
            }
        }
        return person;
    }

    @Override
    public Kino getKinoById(String kinoId) {
        Document document = cinemaCollection.find(eq("kinoid", kinoId)).first();
        Kino kino = new Kino(
                document.getString("kinoname"),
                document.getString("street"),
                document.getString("location"),
                document.getString("zipcode"),
                document.getString("country"),
                document.getString("website"),
                document.getString("telephonenumber"));
        if (document.get("saal") != null) {
            ArrayList<Document> saaldocumentlist = (ArrayList<Document>) document.get("saal");
            for (Document saalDocument : saaldocumentlist) {
                Saal newsaal = new Saal(
                        saalDocument.getInteger("saalnumber"),
                        saalDocument.getInteger("numberofseats"),
                        saalDocument.getString("technology"));
                kino.addSaaltoRooms(newsaal);

            }
        }
        return  kino;
    }

    @Override
    public Movie getMovieById(String movieId) {
        Document document = movieCollection.find(eq("movieid", Integer.parseInt(movieId))).first();
        Movie movie = new Movie();
        movie.setTitle(document.getString("title"));
        movie.setGenre(Movie.Genre.valueOf(document.getString("genre")));
        movie.setRating(document.getInteger("rating"));
        movie.setId(movieId);
        if (document.get("film") != null) {
            ArrayList<Document> filmDocumentList = (ArrayList<Document>) document.get("film");
            for (Document filmDocument : filmDocumentList) {
                Kino kino = new Kino();
                kino.setName(filmDocument.getString("kinoid"));


                Saal saal = new Saal();
                saal.setSaalnumber(filmDocument.getInteger("saalnumber"));
                saal.setKinolocation(kino);

                Film film = new Film(
                        filmDocument.getDate("starttime"),
                        filmDocument.getDate("endtime"),
                        saal, movie);
                film.setFreeseats(filmDocument.getInteger("freeseats"));
                film.setId(filmDocument.getInteger("filmid")+"");
                movie.addFilm(film);
            }
        }
        return movie;
    }

    //not needed
    @Override
    public List<Staff> getStaffOfKinobyKinoID(String kinoId) {
        return null;
    }

    //not needed
    @Override
    public List<Owner> getOwnersOfKinobyKinoID(String kinoId) {
        return null;
    }

    @Override
    public List<Saal> getSaalsofKinobyKinoID(String kinoid) {
        Kino k = getKinoById(kinoid);
        return k.getRooms();
    }

    //not needed
    @Override
    public Person getPersonwithFriendsbyEmail(String email) {
        return getPersonbyID(email);
    }
    @Override
    public String createFilm(Film film) {
        movieCollection.updateOne(eq("movieid", film.getMovie().getId()), new Document("film", new Document()
                .append("filmid", film.hashCode())
                .append("starttime", new Date(film.getBeginn().getTime()))
                .append("endtime", new Date(film.getEnd().getTime()))
                .append("freeseats",film.getFreeseats())
                .append("saalnumber",film.getSaal().getSaalnumber())));

        return Integer.toString(film.hashCode());

    }

    @Override
    public String createPerson(Person person) throws Exception{
        if (personCollection.find(eq("email", person.getEmail())).first() != null) {
            throw new Exception("Person with email " + person.getEmail() + " already exist");
        }
        Document document = new Document("email", person.getEmail())
                .append("firstname", person.getFirstname())
                .append("surname", person.getSurname())
                .append("country", person.getCountry())
                .append("location", person.getLocation())
                .append("zipcode", person.getZipcode())
                .append("street", person.getStreet())
                .append("passwordhash", person.getPasswordhash());
        if (person.getFriends().size() > 0) {
            List<Document> documentfriendlist = new ArrayList<Document>();
            for (Person friend : person.getFriends()) {
                documentfriendlist.add(new Document("friends_email", friend.getEmail()));
            }
            document.append("friends", documentfriendlist);
        }
        if (person.getTickets().size() > 0) {
            List<Document> documentTicketsList = new ArrayList<Document>();
            for (Ticket ticket : person.getTickets()) {
                documentTicketsList.add(new Document("ticket_id", ticket.hashCode())
                        .append("creditcardnumber", ticket.getCreditcardnumber())
                        .append("creditcardnumber", ticket.getCreditcardnumber())
                        .append("paymentdate", new Date(ticket.getPaymentdate().getTime()))
                        .append("filmname", ticket.getFilm().getMovie().getTitle())
                        .append("kinoname",ticket.getFilm().getSaal().getKinolocation().getName())
                        .append("saalnumber",ticket.getFilm().getSaal().getSaalnumber())
                        .append("starttime",new Date(ticket.getFilm().getBeginn().getTime()))
                        .append("film_id",ticket.getFilm().getId()));
            }
            document.append("tickets", documentTicketsList);
        }
        personCollection.insertOne(document);
        return person.getEmail();
    }

    @Override
    public String createKino(Kino kino) {
        if (cinemaCollection.find(eq("kinoid", kino.getId())).first() != null) System.out.println("Duplicate!");
        Document document = new Document("kinoid", kino.hashCode())
                .append("kinoname", kino.getName())
                .append("street", kino.getStreet())
                .append("location", kino.getLocation())
                .append("zipcode", kino.getZipcode())
                .append("country", kino.getCountry())
                .append("website", kino.getWebsite())
                .append("telephonenumber", kino.getTelephonenumber());
        if (kino.getRooms().size() > 0) {
            List<Document> documentSaalList = new ArrayList<Document>();
            List<Saal> saalList = kino.getRooms();
            for (Saal saal : saalList) {
                try {
                    documentSaalList.add(new Document()
                            .append("saalnumber", saal.getSaalnumber())
                            .append("numberofseats", saal.getNumberofseats())
                            .append("technology", saal.getTechnoloy()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            document.append("saal", documentSaalList);
        }
        cinemaCollection.insertOne(document);
        return kino.getId();
    }

    @Override
    public String createOwner(Owner owner) throws Exception {
        Document persondocument = personCollection.find(eq("email",owner.getPerson().getEmail())).first();
        if (persondocument==null) {
            createPerson(owner.getPerson());
            persondocument = personCollection.find(eq("email",owner.getPerson().getEmail())).first();
        }
        Document ownerdocument = new Document("iban",owner.getIban())
                .append("telephone",owner.getTelephone());
        if (owner.getPossessions().size()>0) {
            Document possessionsdocument = new Document();
            for (Kino kino : owner.getPossessions()) {
                possessionsdocument.append("possessions_id",kino.getId());
            }
            ownerdocument.append("possessions",possessionsdocument);
        }
        personCollection.updateOne(persondocument,new Document("$set",ownerdocument));
        return owner.getIban();
    }

    @Override
    public String createSaal(Saal saal) {
        cinemaCollection.updateOne(eq("kinoid", saal.getKinolocation().getId()), new Document("saal", new Document()
                .append("saalnumber", saal.getSaalnumber())
                .append("numberofseats", saal.getNumberofseats())
                .append("technology", saal.getTechnoloy())));
        return Integer.toString(saal.getSaalnumber());
    }

    @Override
    public String createStaff(Staff staff) throws Exception {
        Document persondocument = personCollection.find(eq("email",staff.getPerson().getEmail())).first();
        if (persondocument==null) {
            createPerson(staff.getPerson());
            persondocument = personCollection.find(eq("email",staff.getPerson().getEmail())).first();
        }
        Document staffdocument = new Document("staff_id",staff.hashCode())
                .append("sex",staff.getSex())
                .append("salary",staff.getSalary())
                .append("worklocation",staff.getWorklocation().getId());
        personCollection.updateOne(persondocument,new Document("$set",staffdocument));
        return Integer.toString(staff.hashCode());
    }

    @Override
    public String createTicket(Ticket ticket) {
        Document persondocument = personCollection.find(eq("email",ticket.getOwner().getEmail())).first();
        Document ticketdocument = new Document("tickets", new Document()
                .append("ticket_id", ticket.hashCode())
                .append("creditcardnumber", ticket.getCreditcardnumber())
                .append("paymentdate", new Date(ticket.getPaymentdate().getTime()))
                .append("filmname", ticket.getFilm().getMovie().getTitle())
                .append("kinoname",ticket.getFilm().getSaal().getKinolocation().getName())
                .append("saalnumber",ticket.getFilm().getSaal().getSaalnumber())
                .append("starttime",ticket.getFilm().getBeginn())
                .append("film_id",ticket.getFilm().getId()));

        personCollection.updateOne(persondocument,new Document("$push",ticketdocument));
        return Integer.toString(ticket.hashCode());
    }

    @Override
    public String createMovie(Movie movie) {
        if (movieCollection.find(eq("movieid",movie.getId())).first()!=null) System.out.println("Duplicate!");
        Document document = new Document("movieid",movie.hashCode())
                .append("title", movie.getTitle())
                .append("genre", movie.getGenre().toString())
                .append("rating", movie.getRating());
        if(movie.getFilmList().size()>0){
            List<Document> documentFilmList = new ArrayList<Document>();
            List<Film> filmList = movie.getFilmList();
            for (Film film : filmList) {
                try {
                    documentFilmList.add(new Document()
                            .append("filmid", film.hashCode())
                            .append("starttime", new Date(film.getBeginn().getTime()))
                            .append("endtime", new Date(film.getEnd().getTime()))
                            .append("freeseats",film.getFreeseats())
                            .append("saalnumber",film.getSaal().getSaalnumber())
                            .append("kinoid",film.getSaal().getKinolocation().getId()));

                } catch (Exception e) {e.printStackTrace();}
            }
            document.append("film",documentFilmList);
        }
        movieCollection.insertOne(document);
        return Integer.toString(movie.hashCode());
    }

    @Override
    public void deleteFilm(final Film film) {
        movieCollection.deleteOne(eq("movieid", film.getMovie().getId()));
        Block<Document> documentmapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                Document update = new Document("film", new Document("filmid", film.getId()));
                System.out.println(update.toJson());
                movieCollection.updateOne(document, new Document("$pull", update));
            }
        };
        personCollection.find(eq("film", new Document("film", film.getId()))).forEach(documentmapper);
    }

    //not needed
    @Override
    public void deleteKino(Kino kino) {

    }

    //not needed
    @Override
    public void deleteOwner(Owner owner) {

    }

    @Override
    public void deletePerson(final Person person) {
        personCollection.deleteOne(eq("email", person.getEmail()));
        //Delete Person from FriendsList
        Block<Document> documentmapper = new Block<Document>() {
            @Override
            public void apply(Document document) {
                Document update = new Document("friends", new Document("friends_email", person.getEmail()));
                System.out.println(update.toJson());
                personCollection.updateOne(document, new Document("$pull", update));
            }
        };
        personCollection.find(eq("friends", new Document("friends_email", person.getEmail()))).forEach(documentmapper);
    }

    //not needed
    @Override
    public void deleteSaal(Saal saal) {

    }

    //not needed
    @Override
    public void deleteStaff(Staff staff) {

    }

    @Override
    public void deleteTicket(Ticket ticket) {
        Document persondocument = personCollection.find(eq("email",ticket.getOwner().getEmail())).first();
        List<Document> documentTicketList = (List<Document>) persondocument.get("tickets");
        Document documentTickettoDelete = null;
        for (Document documentTicket:documentTicketList) {
            if (Integer.toString(documentTicket.getInteger("ticket_id")).equals(ticket.getId())) documentTickettoDelete = documentTicket;
        }
        Document update = new Document("tickets", documentTickettoDelete);
        personCollection.updateOne(persondocument, new Document("$pull", update));
    }

    @Override
    public void deleteMovie(Movie movie) {
        cinemaCollection.deleteOne(eq("movieid", movie.getId()));
    }


    @Override
    public void updateFilm(Film film) {
        Document moviedocument = movieCollection.find(eq("movieid",film.getMovie().getId())).first();
        Document documenttoinsert = new Document(
                "film", new Document()
                .append("filmid", film.hashCode())
                .append("starttime", new Date(film.getBeginn().getTime()))
                .append("endtime", new Date(film.getEnd().getTime()))
                .append("freeseats",film.getFreeseats())
                .append("saalnumber",film.getSaal().getSaalnumber())
                .append("kinoid",film.getSaal().getKinolocation().getName()));
        personCollection.updateOne(moviedocument,new Document("$set",documenttoinsert));
    }

    //not needed
    @Override
    public void updateKino(Kino kino) {

    }

    //not needed
    @Override
    public void updateOwner(Owner owner) {

    }

    @Override
    public void updatePerson(Person person) throws Exception {
        if (personCollection.find(eq("email", person.getEmail())).first() == null) {
            throw new Exception("There is no person in database to update");
        }
        Document persondocument = personCollection.find(eq("email",person.getEmail())).first();
        Document documenttoinsert = new Document("email", person.getEmail())
                .append("firstname", person.getFirstname())
                .append("surname", person.getSurname())
                .append("country", person.getCountry())
                .append("location", person.getLocation())
                .append("zipcode", person.getZipcode())
                .append("street", person.getStreet())
                .append("passwordhash", person.getPasswordhash());
        if (person.getFriends().size() > 0) {
            List<Document> documentfriendlist = new ArrayList<Document>();
            for (Person friend : person.getFriends()) {
                documentfriendlist.add(new Document("friends_email", friend.getEmail()));
            }
            documenttoinsert.append("friends", documentfriendlist);
        }
        if (person.getTickets().size() > 0) {
            List<Document> documentTicketsList = new ArrayList<Document>();
            for (Ticket ticket : person.getTickets()) {
                documentTicketsList.add(new Document("ticket_id", ticket.hashCode())
                        .append("creditcardnumber", ticket.getCreditcardnumber())
                        .append("creditcardnumber", ticket.getCreditcardnumber())
                        .append("paymentdate", ticket.getPaymentdate())
                        .append("filmname", ticket.getFilm().getMovie().getTitle())
                        .append("kinoname",ticket.getFilm().getSaal().getKinolocation().getName())
                        .append("saalnumber",ticket.getFilm().getSaal().getSaalnumber())
                        .append("starttime",ticket.getFilm().getBeginn())
                        .append("film_id",ticket.getFilm().getId()));
            }
            documenttoinsert.append("tickets", documentTicketsList);
        }
        personCollection.updateOne(persondocument,new Document("$set",documenttoinsert));

    }

    //not needed
    @Override
    public void updateSaal(Saal saal) {

    }

    //not needed
    @Override
    public void updateStaff(Staff staff) {

    }

    //not needed
    @Override
    public void updateTicket(Ticket ticket) {

    }

    @Override
    public void updateMovie(Movie movie) throws Exception {
        if (movieCollection.find(eq("movieid", movie.getId())).first() == null) {
            throw new Exception("There is no movie in database to update");
        }
        Document movieDocument = movieCollection.find(eq("movieid",movie.hashCode())).first();
        Document document = new Document("movieid",movie.hashCode())
                .append("title", movie.getTitle())
                .append("genre", movie.getGenre().name())
                .append("rating", movie.getRating());
        if(movie.getFilmList().size()>0){
            List<Document> documentFilmList = new ArrayList<Document>();
            List<Film> filmList = movie.getFilmList();
            for (Film film : filmList) {
                try {
                    documentFilmList.add(new Document()
                            .append("filmid", film.hashCode())
                            .append("starttime", new Date(film.getBeginn().getTime()))
                            .append("endtime", new Date(film.getEnd().getTime()))
                            .append("freeseats",film.getFreeseats())
                            .append("saalnumber",film.getSaal().getSaalnumber())
                            .append("kinoid",film.getSaal().getKinolocation().getId()));

                } catch (Exception e) {e.printStackTrace();}
            }
            document.append("film",documentFilmList);
        }
        movieCollection.updateOne(movieDocument,new Document("$set",document));
    }

    @Override
    public Movie getMovieWithFilmTicketsById(String movieId) {
        Document document = movieCollection.find(eq("movieid", movieId)).first();
        Movie movie = new Movie(
                document.getString("title"),
                Movie.Genre.valueOf(document.getString("genre")),
                document.getInteger("rating")
        );
        if (document.get("film") != null) {
            ArrayList<Document> filmDocumentList = (ArrayList<Document>) document.get("film");
            for (Document filmDocument : filmDocumentList) {
                Kino kino = new Kino();
                kino.setName(filmDocument.getString("kinoname"));


                movie.setTitle(filmDocument.getString("filmname"));
                Saal saal = new Saal();
                saal.setSaalnumber(filmDocument.getInteger("saalnumber"));
                saal.setKinolocation(kino);

                Film film = new Film(
                        filmDocument.getDate("starttime"),
                        filmDocument.getDate("endtime"),
                        saal, movie);
                film.setTickets(getTicketsForFilm(film.getId()));
            }
        }
        return movie;
    }

    @Override
    public void dropAll() {
        personCollection.drop();
        cinemaCollection.drop();
        movieCollection.drop();
    }
}
