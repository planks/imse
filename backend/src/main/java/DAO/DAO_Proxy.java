package DAO;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DAO_Proxy {
    DAO dao;
    private static DAO_Proxy instance;


    DAO_Proxy(){
        this.dao = new DAO_Impl();
    }

    /**
     * Classical Singleton implementation
     */
    public static DAO_Proxy getInstance () {
        if (DAO_Proxy.instance == null) {
            DAO_Proxy.instance = new DAO_Proxy();
        }
        return DAO_Proxy.instance;
    }

    public void changeToMongoDB(){
        this.dao = new DAO_Impl_Mongo();
    }

    public DAO getDao() {
        return dao;
    }

    public void setDao(DAO dao) {
        this.dao = dao;
    }
}
