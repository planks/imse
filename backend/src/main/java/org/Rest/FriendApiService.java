package org.Rest;

import Management.FriendMgmt;
import POJO.Person;

import javax.ws.rs.core.Response;

public abstract class FriendApiService {

    public abstract Response findFriendviaEmail(String email)  ;
    public abstract Response findFriendviaName(String forename, String lastname);
    public abstract Response getallPerson();
    public abstract Response addFriends(FriendMgmt person);
    public abstract Response getFriends(String email);
    public abstract Response removeFriend(String email);
    public abstract Response removeFriend(FriendMgmt friend);
}
