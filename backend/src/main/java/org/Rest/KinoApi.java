package org.Rest;

import Management.Kinomanagement;
import POJO.Kino;
import POJO.Saal;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/kino")
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaJerseyServerCodegen", date = "2018-12-28T09:13:33.810Z[GMT]")public class KinoApi  {
    private Kinomanagement delegate;
    Gson gson = new GsonBuilder().create();
    public KinoApi(){
        this.delegate = Kinomanagement.getInstance();
    }

    @POST
    public Response addKino(Kino body)
    {
        return delegate.addKino(body);
    }

    @POST
    @Path("/{kinoId}/saal")
    public Response addSaal(@PathParam("kinoId") String kinoId, Saal body){
        return delegate.addSaal(kinoId, body);
    }

    @DELETE
    @Path("/{kinoId}")
    public Response deleteKino(@PathParam("kinoId") String kinoId)
    {
        return delegate.removeKino(kinoId);
    }

    @GET
    @Path("/{kinoId}")
    @Produces({ "application/json", "application/xml" })
    public Response getKinoById(@PathParam("kinoId") String kinoId)
    {
        return delegate.findKinoById(kinoId);
    }

    @GET
    @Path("/{kinoId}/saal")
    @Produces({ "application/json", "application/xml" })
    public Response getSaalsForKino(@PathParam("kinoId") String kinoId)
    {
        return delegate.findSaalsOfKino(kinoId);
    }

    @GET
    @Path("/{kinoId}/staff")
    @Produces({ "application/json", "application/xml" })
    public Response getStaffOfKino(@PathParam("kinoId") String kinoId)
    {
        return delegate.findStaffOfKino(kinoId);
    }

    @GET
    @Path("/{kinoId}/owner")
    @Produces({ "application/json", "application/xml" })
    public Response getOwnerOfKino(@PathParam("kinoId") String kinoId)
    {
        return delegate.findOwnerOfKino(kinoId);
    }

    @GET
    @Produces({ "application/json", "application/xml" })
    public Response getAllKino()
    {
        return delegate.findAllKinos();
    }


    @GET
    @Path("/saals/{kinoId}/saal/{saalId}")
    @Produces({ "application/json", "application/xml" })
    public Response getSaalsOfKino(@PathParam("kinoId")String kinoId)
    {
        return delegate.findSaalsOfKino(kinoId);
    }
    @PUT
    public Response updateKino(Kino body)
    {
        return delegate.updateKino(body   );
    }
}
