package org.Rest;

import DAO.DAO_Impl;
import POJO.Film;
import POJO.Saal;
import POJO.Ticket;
import Webservice.ApiResponseMessage;
import impl.FilmApiServiceImpl;
import impl.TicketApiServiceImpl;

import javax.ws.rs.core.Response;
import java.util.List;

public abstract class FilmApiService {

    public abstract Response createFilm(Film film  );

    public abstract Response cancelFilm(String filmId);

    public abstract Response updateFilm(Film film  );

    public abstract Response getTicketsForFilm(String film  )  ;

    public abstract Response findFilmById(String filmId);

    public abstract Response getAllFilms();
}
