package org.Rest;

import Management.FriendMgmt;
import POJO.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import impl.FriendApiServiceImpl;
import impl.UserApiServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/friend")
public class FriendApi {

    private FriendApiService delegate;
    Gson gson = new GsonBuilder().create();

    public FriendApi(){
        this.delegate = FriendApiServiceImpl.getInstance();
    }

    @GET
    @Path("/findFriendEmail/{email}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response findFriendviaEmail(@PathParam("email") String email)
    {
        return delegate.findFriendviaEmail(email);
    }

    @GET
    @Path("/findFriendName/{forename}/{lastname}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response findFriendviaEmail(@PathParam("forename") String forename,@PathParam("lastname")String lastname)
    {
        return delegate.findFriendviaName(forename,lastname);
    }
    @GET
    @Path("/personlist")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getallPerson()
    {
        return delegate.getallPerson();
    }

    @GET
    @Path("/friendList/{email}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getAddedFriends(@PathParam("email") String email)
    {
        return delegate.getFriends(email);
    }

    @POST
    @Path("/add/{friendEmail}/{mymail}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response addFriend(@PathParam("friendEmail") String friendEmail, @PathParam("mymail") String mymail)
    {
        FriendMgmt friend=new FriendMgmt(friendEmail,mymail);
        return delegate.addFriends(friend);
    }

    @DELETE
    @Path("/delFriend/{friendEmail}/{mymail}")
    public Response deleteFriend(@PathParam("friendEmail") String friendEmail, @PathParam("mymail") String mymail)
    {
        FriendMgmt friend=new FriendMgmt(friendEmail,mymail);
        return delegate.removeFriend(friend);
    }

    @POST
    @Path("/add")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response addFriends(String body)
    {
        FriendMgmt t = gson.fromJson(body, FriendMgmt.class);
        return delegate.addFriends(t);
    }

}
