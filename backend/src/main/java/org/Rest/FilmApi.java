package org.Rest;

import POJO.Film;
import POJO.Saal;
import POJO.Ticket;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import impl.FilmApiServiceImpl;
import impl.TicketApiServiceImpl;


import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/film")
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaJerseyServerCodegen", date = "2018-12-28T09:13:33.810Z[GMT]")public class FilmApi  {
    private FilmApiServiceImpl delegate;
    Gson gson = new GsonBuilder().create();
    public FilmApi(){
        this.delegate = FilmApiServiceImpl.getInstance();
    }

    @POST
    @Path("/{filmId}/buyticket")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response buyTicket(Ticket body)
    {
        return TicketApiServiceImpl.getInstance().buyTicket(body);

    }

    @DELETE
    @Path("/{filmId}")
    public Response deleteFilm(@PathParam("filmId") String filmId)
      {
        return delegate.cancelFilm(filmId);
    }

    @GET
    @Produces({ "application/json", "application/xml" })
    public Response getAllFilms()
    {
        return delegate.getAllFilms();
    }

    @GET
    @Path("/{filmId}")
    @Produces({ "application/json", "application/xml" })
    public Response getFilmById(@PathParam("filmId") String filmId)
      {
        return delegate.findFilmById(filmId   );
    }
    @PUT
    public Response updateFilm(Film body)
      {
        return delegate.updateFilm(body   );
    }

    @GET
    @Path("/{filmId}/tickets")
    @Produces({ "application/json", "application/xml" })
    public Response getTicketsForFilm(@PathParam("filmId") String filmId)
    {
        return delegate.getTicketsForFilm(filmId);
    }
}
