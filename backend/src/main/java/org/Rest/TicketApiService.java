package org.Rest;


import POJO.Ticket;

import javax.ws.rs.core.Response;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaJerseyServerCodegen", date = "2018-12-28T09:13:33.810Z[GMT]")public abstract class TicketApiService {
    public abstract Response buyTicket(Ticket body  )  ;
    public abstract Response deleteTicket(String ticket  )  ;
    public abstract Response getTicketById(String ticketId)  ;
    public abstract Response getTicketsForPerson(String personEmail);

    public abstract Response getAllTickets();

    public abstract Response updateTicket(Ticket body);
}
