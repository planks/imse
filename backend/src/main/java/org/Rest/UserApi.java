package org.Rest;

import DataFiller.Temp;
import POJO.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import impl.TicketApiServiceImpl;
import impl.UserApiServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/user")
public class UserApi {

    private UserApiServiceImpl delegate;
    Gson gson = new GsonBuilder().create();

    public UserApi(){
        this.delegate = UserApiServiceImpl.getInstance();
    }


    @POST
    @Path("/add")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response addPerson(String body)
    {
        Person t = gson.fromJson(body, Person.class);
        return delegate.addUser(t);

    }

    @GET
    @Path("/{userEmail}/ticket")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getTicketsForPerson(@PathParam("userEmail") String userEmail)
    {
        return TicketApiServiceImpl.getInstance().getTicketsForPerson(userEmail);
    }

    @DELETE
    @Path("/{delUser}")
    public Response deleteTicket(@PathParam("delUser") String email)
    {
        return delegate.deleteUser(email);
    }

    @POST
    @Path("/login")
    //@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces({ MediaType.APPLICATION_JSON })
    public Response loginUser(@FormParam("username") String username,@FormParam("password") String password)
    {
        return delegate.loginUser(username,password);
    }
    @GET
    @Path("/testLogin/{email}/{pwd}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response testLogin(@PathParam("email") String email,@PathParam("pwd")String pwd)
    {
        return delegate.loginUser(email,pwd);
    }

    @PUT
    @Path("/updatePerson")
    @Produces({"application/json", "application/xml"})
    public Response updateFilm(Person body) {
        return delegate.updatePerson(body);
    }

    @GET
    @Path("/getPersonwithMail/{email}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response findFriendviaEmail(@PathParam("email") String email)
    {
        return delegate.findFriendviaEmail(email);
    }
    @GET
    @Path("/getPersonwithName/{forename}/{lastname}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response findFriendviaEmail(@PathParam("forename") String forename,@PathParam("lastname")String lastname)
    {
        return delegate.findFriendviaName(forename,lastname);
    }

    @POST
    @Path("/userLogin")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response userLogin(String body)
    {
        Temp t = gson.fromJson(body, Temp.class);
        String user=t.getUser();
        String pwd=t.getPwd();
        return delegate.loginUser(user,pwd);
    }
}
