package org.Rest;



import POJO.Ticket;
import impl.TicketApiServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/ticket")
public class TicketApi  {
    private TicketApiServiceImpl delegate;
    public TicketApi(){
        this.delegate = TicketApiServiceImpl.getInstance();
   }


    @DELETE
    @Path("/{ticketId}")
    public Response deleteTicket(@PathParam("ticketId") String ticketId)
    {
        return delegate.deleteTicket(ticketId);
    }

    @GET
    @Path("/{ticketId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getTicketById(@PathParam("ticketId") String ticketId)
    {
        return delegate.getTicketById(ticketId);
    }

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getAllTickets()
    {
        return delegate.getAllTickets();
    }

    @PUT
    @Produces({"application/json", "application/xml"})
    public Response updateTicket(Ticket body) {
        return delegate.updateTicket(body);
    }
}