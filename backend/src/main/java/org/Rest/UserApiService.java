package org.Rest;

import POJO.Person;
import javax.ws.rs.core.Response;

public abstract class UserApiService  {
    public abstract Response addUser(Person body  )  ;
    public abstract Response updatePerson(Person person);
    public abstract Response deleteUser(String email)  ;
    public abstract Response findFriendviaEmail(String email)  ;
    public abstract Response findFriendviaName(String forename, String lastname);
    public abstract Response loginUser(String email,String password);
}
