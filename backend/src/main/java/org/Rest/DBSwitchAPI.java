package org.Rest;

import DAO.DAO_Proxy;
import DataFiller.DataFiler;
import Webservice.ApiResponseMessage;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/switchStorageBackendImplementation")
public class DBSwitchAPI {

    @PUT
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response switchToMonogDB()
    {
        DataFiler.migrateDB();
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Switched to MongoDB")).build();
    }
}

