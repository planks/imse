package org.Rest;

import POJO.Film;
import POJO.Movie;
import Webservice.ApiResponseMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import impl.FilmApiServiceImpl;
import impl.MovieApiServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;


@Path("/movie")
public class MovieApi {

    private MovieApiServiceImpl delegate;
    Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PROTECTED).create();

    public MovieApi() {
        this.delegate = MovieApiServiceImpl.getInstance();
    }

    @POST
    public Response addMovie(Movie body) {
        return delegate.createMovie(body);
    }

    @POST
    @Path("/{movieId}")
    public Response addFilmForMovie(Film body) {
        return FilmApiServiceImpl.getInstance().createFilm(body);
    }

    @DELETE
    @Path("/{movieId}")
    public Response deleteMovir(@PathParam("movieId") String filmId) {
        return delegate.cancelMovie(filmId);
    }

    @GET
    @Produces({"application/json", "application/xml"})
    public Response findAllMovies() {
        return delegate.findAllMovies();
    }

    @GET
    @Path("/{movieId}")
    @Produces({"application/json", "application/xml"})
    public Response getMovieById(@PathParam("movieId") String filmId) {
        System.out.println("movieId23 = [" + filmId + "]");
        return delegate.getMovieById(filmId);
    }

    @GET
    @Path("/{movieId}/films")
    @Produces({"application/json", "application/xml"})
    public Response findFilmsForMovie(@PathParam("movieId") String filmId) {
        return delegate.findFilmsForMovie(filmId);
    }

    @PUT
    @Produces({"application/json", "application/xml"})
    public Response updateFilm(Movie body) {
        return delegate.updateMovie(body);
    }

    @GET
    @Path("/query")
    @Produces({"application/json", "application/xml"})
    public Response findMoviesByGenre(
            @DefaultValue("") @QueryParam("genre") String genre,
            @DefaultValue("") @QueryParam("title") String title,
            @DefaultValue("9999") @QueryParam("rating") int rating) {
        List<Movie> overAll = new ArrayList<>();
        List<Movie> genreList = null;
        List<Movie> ratingList = null;
        List<Movie> titleList = null;
        if (!genre.equals("")){
            genreList = new ArrayList<>();
            genreList.addAll(delegate.findMovieByGenre(genre));
            overAll.addAll(genreList);
        }
        if (!title.equals("")){
            titleList = new ArrayList<>();
            titleList.addAll(delegate.findMoviesByTitle(title));
            if (genreList != null){
                if (titleList.size() > 0){
                    for (Movie m : genreList){
                        for (Movie m1 : titleList){
                            if (!m.getTitle().equals(m1.getTitle())){
                                overAll.remove(m);
                            }
                        }
                    }
                }else {
                    overAll.removeAll(overAll);
                }
            }else {
                overAll.addAll(titleList);
            }
        }
        if (rating != 9999){
            ratingList = new ArrayList<>();
            ratingList.addAll(delegate.findMovieByRating(rating));
            if (genreList != null){
                for (Movie m : genreList){
                    if (m.getRating() != rating){
                        overAll.remove(m);
                    }
                }
            }else if (titleList != null && titleList.size() != 0){
                if (titleList.get(0).getRating() != rating){
                    overAll.remove(titleList.get(0));
                }
            }else{
                overAll.addAll(ratingList);
            }
        }

        String liststring = gson.toJson(overAll);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, liststring)).build();

    }
}
