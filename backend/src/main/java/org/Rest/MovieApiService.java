package org.Rest;


import POJO.Movie;
import com.sun.istack.NotNull;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.core.Response;
import java.util.List;

public abstract class MovieApiService {
    public abstract Response createMovie(Movie movie  );
    public abstract Response cancelMovie(String movieId);
    public abstract List<Movie> findMovieByGenre(String genre)  ;
    public abstract List<Movie> findMovieByRating( Integer ratings  )  ;
    public abstract Response findAllMovies()  ;
    public abstract List<Movie> findMoviesByTitle(String title  )  ;
    public abstract Response getMovieById(String filmId)  ;
    public abstract Response findFilmsForMovie(String movieId);
    public abstract Response updateMovie(Movie movie);
}
