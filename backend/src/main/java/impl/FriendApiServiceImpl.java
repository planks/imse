package impl;

import DAO.*;
import Management.FriendMgmt;
import Management.Personmgmt;
import Management.Usermanagement;
import POJO.Kino;
import POJO.Person;
import Webservice.ApiResponseMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.Rest.FriendApiService;

import javax.ws.rs.core.Response;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class FriendApiServiceImpl extends FriendApiService {
    DAO_Proxy proxy = DAO_Proxy.getInstance();
    Gson gson;
    Personmgmt personmgmt;
    FriendMgmt friendMgmt;

    private static FriendApiServiceImpl instance;

    /**
     * Classical Singleton implementation
     */
    public static FriendApiServiceImpl getInstance () {
        if (FriendApiServiceImpl.instance == null) {
            FriendApiServiceImpl.instance = new FriendApiServiceImpl();
        }
        return FriendApiServiceImpl.instance;
    }

    private FriendApiServiceImpl(){
        gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PROTECTED).create();
        personmgmt=new Personmgmt();
        friendMgmt=new FriendMgmt("test@gmx.at","test@gmx.at");
    }


    @Override
    public Response findFriendviaEmail(String email) {
        Person person = personmgmt.searchFriendMail(email);
        String jsondFindviaMail = gson.toJson(person,Person.class);
        System.out.println(jsondFindviaMail);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, jsondFindviaMail)).build();
    }

    @Override
    public Response findFriendviaName(String forename, String lastname) {
        Person person = personmgmt.searchFriendName(forename,lastname);
        String jsonFindPerson = gson.toJson(person,Person.class);
        System.out.println(jsonFindPerson);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, jsonFindPerson)).build();
    }

    @Override
    public Response getallPerson() {
        List<Person> personList=personmgmt.getAllPerson();
        ArrayList<String> personArrayList=new ArrayList<String >();
        for(Person person : personList){
            personArrayList.add(person.getEmail());
        }
        String jsonFindPerson = gson.toJson(personArrayList);
        try {
            gson.excluder().excludeField(Person.class.getDeclaredField("email"),false);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        System.out.println(jsonFindPerson);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, jsonFindPerson)).build();
    }

    @Override
    public Response addFriends(FriendMgmt person) {
        friendMgmt.setFriend(person);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "Add Friends")).build();
    }

    @Override
    public Response getFriends(String email) {
        List<String> list=friendMgmt.getFriends(email);
        String jsonGetFriends = gson.toJson(list);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK,jsonGetFriends )).build();
    }

    @Override
    public Response removeFriend(String email) {
        Personmgmt personmgmt=new Personmgmt();
        List<String> personList=friendMgmt.getFriends(email);
        for(int i=0; i< personList.size(); i++);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "delete the friend!")).build();

    }

    @Override
    public Response removeFriend(FriendMgmt friend) {
        friendMgmt.removeFriend(friend);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "delete the friend!")).build();
    }
}
