package impl;

import DAO.DAO;
import Management.Usermanagement;
import POJO.Film;
import POJO.Person;
import Webservice.ApiResponseMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.Rest.UserApiService;
import DAO.DAO;
import DAO.DAO_Proxy;
import DAO.DAO_Impl;

import javax.ws.rs.core.Response;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class UserApiServiceImpl extends UserApiService {
    DAO_Proxy proxy = DAO_Proxy.getInstance();
    Gson gson;
    Usermanagement usermanagement;

    private static UserApiServiceImpl instance;

    /**
     * Classical Singleton implementation
     */
    public static UserApiServiceImpl getInstance () {
        if (UserApiServiceImpl.instance == null) {
            UserApiServiceImpl.instance = new UserApiServiceImpl();
        }
        return UserApiServiceImpl.instance;
    }

    private UserApiServiceImpl(){
        gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PROTECTED).create();
        usermanagement=new Usermanagement();
    }

    @Override
    public Response addUser(Person body) {
        try {
            proxy.getDao().createPerson(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "create User!")).build();
    }

    @Override
    public Response updatePerson(Person person) {
        try {
            proxy.getDao().updatePerson(person);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }

    @Override
    public Response deleteUser(String  email) {
        List<Person> personList=proxy.getDao().getallPersons();
        for(Person p:personList){
            if(p.getEmail().equals(email)) proxy.getDao().deletePerson(p);
        }
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }

    @Override
    public Response findFriendviaEmail(String email) {
        Person person = usermanagement.searchFriendMail(email);
        String jsondFindviaMail = gson.toJson(person,Person.class);

        System.out.println(jsondFindviaMail);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, jsondFindviaMail)).build();
    }

    @Override
    public Response findFriendviaName(String forename, String lastname) {
        Person person = usermanagement.searchFriendName(forename,lastname);
        String jsonFindPerson = gson.toJson(person,Person.class);
        System.out.println(jsonFindPerson);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, jsonFindPerson)).build();
    }

    @Override
    public Response loginUser(String email, String password) {
       Person p=usermanagement.loginUser(email,password);

           String personFind = gson.toJson(p,Person.class);
           System.out.println(personFind);
           return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, personFind)).build();
    }
}
