package impl;


import DAO.DAO_Impl;
import DAO.DAO_Proxy;
import DAO.DAO_Impl_Mongo;
import POJO.Film;
import POJO.Person;
import POJO.Ticket;
import Webservice.ApiResponseMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.Rest.TicketApiService;

import javax.ws.rs.core.Response;
import java.lang.reflect.Modifier;
import java.util.List;


public class TicketApiServiceImpl extends TicketApiService {

    DAO_Proxy proxy = DAO_Proxy.getInstance();
    Gson gson;

    private static TicketApiServiceImpl instance;

    /**
     * Classical Singleton implementation
     */
    public static TicketApiServiceImpl getInstance () {
        if (TicketApiServiceImpl.instance == null) {
            TicketApiServiceImpl.instance = new TicketApiServiceImpl();
        }
        return TicketApiServiceImpl.instance;
    }

    private TicketApiServiceImpl(){
        gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PROTECTED).create();
    }

    @Override
    public Response buyTicket(Ticket ticket)   {
        Film currentFilm = proxy.getDao().getFilmForBuyingTicketsByFilmId(ticket.getFilm().getId());
        if (currentFilm.getTickets() == null){
            currentFilm = proxy.getDao().getFilmById(currentFilm.getId());
        }
        Person currentPerson = proxy.getDao().getPersonForBuyingTicketByPersonId(ticket.getOwner().getEmail());
        try {
            currentFilm.addTicket(ticket);
            currentPerson.addTicket(ticket);
            if (DAO_Impl.class.isInstance(proxy.getDao()))
                proxy.getDao().updateFilm(currentFilm);
            if (DAO_Impl_Mongo.class.isInstance(proxy.getDao())) {
                ticket.setFilm(currentFilm);
                ticket.setOwner(currentPerson);
                String id =  proxy.getDao().createTicket(ticket);
                return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK,id)).build();
            }
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, ticket.getId())).build();
        }catch (Exception e){
            e.printStackTrace();
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "No Tickets available")).build();
        }
    }
    @Override
    public Response deleteTicket(String ticketID )   {
        Ticket ticket = proxy.getDao().getTicketByID(ticketID);
        Film currentFilm = proxy.getDao().getFilmWithSaalsAndTicketsForDeletingFilmByFilmId(ticket.getFilm().getId());
        Person currentPerson = proxy.getDao().getPersonForBuyingTicketByPersonId(ticket.getOwner().getEmail());
        currentFilm.removeTicket(ticket);
        currentPerson.removeTicket(ticket);
        try {
            proxy.getDao().updateFilm(currentFilm);
            proxy.getDao().deleteTicket(ticket);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, ticketID)).build();
        }catch (Exception e){
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Does not exist")).build();
        }
    }
    @Override
    public Response getTicketById(String ticketId)   {
        Ticket ticket = proxy.getDao().getTicketByID(ticketId);
        if (ticket == null){
            return Response.noContent().build();
        }

        String json = gson.toJson(ticket);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, json)).build();
    }

    @Override
    public Response getTicketsForPerson(String personEmail) {
        List<Ticket> ticketList = proxy.getDao().getTicketsForPerson(personEmail);
        if (ticketList.size() == 0){
            return Response.noContent().build();
        }
        String ticketListJSON = gson.toJson(ticketList);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, ticketListJSON)).build();
    }

    @Override
    public Response getAllTickets() {
        List<Ticket> ticketList = proxy.getDao().getallTicket();
        String ticketListJSON = gson.toJson(ticketList);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, ticketListJSON)).build();
    }

    @Override
    public Response updateTicket(Ticket body) {
        try {
            proxy.getDao().updateTicket(body);
            String json = gson.toJson(body);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, json)).build();
        }catch (Exception e){
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Does not exist")).build();
        }
    }

}
