package impl;

import DAO.DAO;
import DAO.DAO_Proxy;
import DAO.DAO_Impl;
import POJO.Film;
import POJO.Movie;
import Webservice.ApiResponseMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.Rest.MovieApiService;

import javax.ws.rs.core.Response;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class MovieApiServiceImpl extends MovieApiService {
    DAO_Proxy proxy = DAO_Proxy.getInstance();
    Gson gson;

    private static MovieApiServiceImpl instance;

    /**
     * Classical Singleton implementation
     */
    public static MovieApiServiceImpl getInstance () {
        if (MovieApiServiceImpl.instance == null) {
            MovieApiServiceImpl.instance = new MovieApiServiceImpl();
        }
        return MovieApiServiceImpl.instance;
    }

    private MovieApiServiceImpl(){
        gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PROTECTED).create();
    }

    @Override
    public Response createMovie(Movie movie) {
        try {
            String id = proxy.getDao().createMovie(movie);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, id)).build();
        }catch (Exception e){
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Already exists")).build();
        }
    }

    @Override
    public Response cancelMovie(String movieId) {
        Movie m = proxy.getDao().getMovieWithFilmTicketsById(movieId);
        try {
            proxy.getDao().deleteMovie(m);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, movieId)).build();
        }catch (Exception e){
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Does not exist")).build();
        }
    }

    @Override
    public List<Movie> findMovieByGenre(String genre) {
        List<Movie> movieList = new ArrayList<>();
        try{
            movieList = proxy.getDao().getMoviesByGenre(Movie.Genre.valueOf(genre));
            return movieList;
        }catch (IllegalArgumentException iae){
            return movieList;
        }
    }

    @Override
    public List<Movie> findMovieByRating(Integer ratings) {
        List<Movie> movieList = proxy.getDao().getMoviesByRating(ratings.intValue());
        return movieList;
    }

    @Override
    public Response findAllMovies() {
        List<Movie> movieList = proxy.getDao().getallMovies();
        String movieListJSON = gson.toJson(movieList);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, movieListJSON)).build();
    }

    @Override
    public List<Movie> findMoviesByTitle(String title) {
        List<Movie> movieList = proxy.getDao().getMoviebyTitle(title);
        return movieList;

    }

    @Override
    public Response getMovieById(String movieId) {
        Movie movie = proxy.getDao().getMovieById(movieId);
        if (movie == null){
            return Response.noContent().build();
        }
        String s = gson.toJson(movie);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, s)).build();

    }
    @Override
    public Response findFilmsForMovie(String movieId) {
        Movie movie = proxy.getDao().getMovieById(movieId);
        List<Film> filmList = movie.getFilmList();

        if (filmList.size() == 0){
            return Response.noContent().build();
        }
        for (Film f : filmList){
            if (f.getFreeseats() <= 0){
                filmList.remove(f);
            }
        }
        String filmListJSON = gson.toJson(filmList);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, filmListJSON)).build();
    }

    @Override
    public Response updateMovie(Movie movie) {
        try {
            proxy.getDao().updateMovie(movie);
            String json = gson.toJson(movie);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, json)).build();
        } catch (Exception e) {
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Does not exist")).build();
        }
    }

}
