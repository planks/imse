package impl;

import DAO.DAO_Impl;
import DAO.DAO_Proxy;
import POJO.Film;
import POJO.Saal;
import POJO.Ticket;
import Webservice.ApiResponseMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.Rest.FilmApiService;

import javax.ws.rs.core.Response;
import java.lang.reflect.Modifier;
import java.util.List;


@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.JavaJerseyServerCodegen", date = "2018-12-28T09:13:33.810Z[GMT]")public class FilmApiServiceImpl extends FilmApiService {
    DAO_Proxy proxy = DAO_Proxy.getInstance();
    Gson gson;

    private static FilmApiServiceImpl instance;

    /**
     * Classical Singleton implementation
     */
    public static FilmApiServiceImpl getInstance () {
        if (FilmApiServiceImpl.instance == null) {
            FilmApiServiceImpl.instance = new FilmApiServiceImpl();
        }
        return FilmApiServiceImpl.instance;
    }

    private FilmApiServiceImpl(){
        gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PROTECTED).create();
    }

    @Override
    public Response createFilm(Film film){
        Saal s = proxy.getDao().getSaalbyKinoandSaalnumber(film.getSaal().getKinolocation().getId(),film.getSaal().getSaalnumber());
        if (s == null){
            return Response.noContent().build();
        }
        s.addFilm(film);
        try {
            String id = proxy.getDao().createFilm(film);
            if (DAO_Impl.class.isInstance(proxy.getDao()))
                proxy.getDao().updateSaal(s);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, id)).build();
        }catch (Exception e){
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Already exists")).build();
        }

    }
    @Override
    public Response cancelFilm(String filmId){
        Film film = proxy.getDao().getFilmWithSaalsAndTicketsForDeletingFilmByFilmId(filmId);
        Saal s = film.getSaal();
        s.removeFilm(film);
        try{
            proxy.getDao().deleteFilm(film);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, filmId)).build();
        }catch (Exception e){
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Does not exist")).build();
        }


    }

    private void cancelTicketsForFilm(Film film) {
        List<Ticket> ticketList = proxy.getDao().getTicketsForFilm(film.getId());
        film.setTickets(ticketList);
        for (Ticket t : ticketList){
            TicketApiServiceImpl.getInstance().deleteTicket(t.getId());
        }
        film.getTickets().removeAll(film.getTickets());
    }

    @Override
    public Response updateFilm(Film film  )  {
        try{
            proxy.getDao().updateFilm(film);
            String json = gson.toJson(film);
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, json)).build();
        }catch (Exception e){
            return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.ERROR, "Does not exist")).build();
        }
    }

    @Override
    public Response getTicketsForFilm(String filmId) {
        List<Ticket> ticketList = proxy.getDao().getTicketsForFilm(filmId);
        if (ticketList.size() == 0){
            return Response.noContent().build();
        }
        String s = gson.toJson(ticketList);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, s)).build();
    }

    @Override
    public Response findFilmById(String filmId) {
        Film film = proxy.getDao().getFilmById(filmId);
        if (film == null){
            return Response.noContent().build();
        }

        String s = gson.toJson(film,Film.class);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, s)).build();
    }

    @Override
    public Response getAllFilms() {
        List<Film> filmList = proxy.getDao().getallFilms();
        if (filmList == null){
            return Response.noContent().build();
        }

        String s = gson.toJson(filmList);
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, s)).build();
    }
}
