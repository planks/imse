package DataFiller;
import DAO.DAO_Proxy;
import POJO.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class DataFiler {

    public static DAO_Proxy proxy = DAO_Proxy.getInstance();
    public static void initialFilling() {


        BufferedReader br = null;
        BufferedReader br2 = null;
        String csvPerson = "Data/person.csv";
        String csvOwner = "Data/owner.csv";
        String csvSaal = "Data/saal.csv";
        String csvMovie = "Data/movie.csv";
        String csvStaff = "Data/staff.csv";
        String csvFilm = "Data/film.csv";
        String csvTicket = "Data/ticket.csv";
        String csvKino = "Data/kino.csv";
        String line = "";
        Random rand = new Random();

        DAO_Proxy proxy = DAO_Proxy.getInstance();

        //Add 3 admins
        Person p0 = new Person("dummy" + 0 + "@gmail.com", "DummyFirstname" + 0, "DummySurname" + 0, "DummyCounrty" + 0, "DummyLocation" + 0, "DummyCountry" + 0, "DummyStreet" + 0, "DummyHash" + 0);
        Person p1 = new Person("dummy" + 1 + "@gmail.com", "DummyFirstname" + 1, "DummySurname" + 1, "DummyCounrty" + 1, "DummyLocation" + 1, "DummyCountry" + 1, "DummyStreet" + 1, "DummyHash" + 1);
        Person p2 = new Person("dummy" + 2 + "@gmail.com", "DummyFirstname" + 2, "DummySurname" + 2, "DummyCounrty" + 2, "DummyLocation" + 2, "DummyCountry" + 2, "DummyStreet" + 2, "DummyHash" + 2);
        try {
            proxy.getDao().createPerson(p0);
            proxy.getDao().createPerson(p1);
            proxy.getDao().createPerson(p2);
        }catch (Exception e){

        }

        try {
            System.out.println("start");
            InputStreamReader is = new InputStreamReader(DataFiler.class.getClassLoader().getResourceAsStream(csvKino));
            br = new BufferedReader(is);
            while ((line = br.readLine()) != null) {
                String[] kino = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                String kinoName = kino[0];
                String kinoStreet = kino[1];
                String kinoLocation = kino[2];
                String kinoZipcode = kino[3];
                String kinoCountry = kino[4];
                String kinoWebsite = kino[5];
                String kinoPhonenumber = kino[6];
                Kino k = new Kino(kinoName, kinoStreet, kinoLocation, kinoZipcode, kinoCountry, kinoWebsite, kinoPhonenumber);
                proxy.getDao().createKino(k);

                br2 = new BufferedReader(new InputStreamReader(DataFiler.class.getClassLoader().getResourceAsStream(csvSaal)));
                while ((line = br2.readLine()) != null) {
                    String[] saal = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    String saalnumber = saal[0];
                    String numberofseats = saal[1];
                    String technology = saal[2];
                    Saal s = new Saal(Integer.parseInt(saalnumber), Integer.parseInt(numberofseats), technology);
                    k.addSaaltoRooms(s);
                    s.setKinolocation(k);
                    proxy.getDao().createSaal(s);
                }
            }
            System.out.println("donekino and saal");
            System.out.println("start movie and film");


            br = new BufferedReader(new InputStreamReader(DataFiler.class.getClassLoader().getResourceAsStream(csvMovie)));
            List<Saal> saals = proxy.getDao().getallSaal();
            while ((line = br.readLine()) != null) {
                String[] movie = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                String title = movie[0];
                String genre = movie[1];
                String rating = movie[2];

                Movie m = new Movie(title, Movie.Genre.valueOf(genre), Integer.parseInt(rating));
                proxy.getDao().createMovie(m);

                br2 = new BufferedReader(new InputStreamReader(DataFiler.class.getClassLoader().getResourceAsStream(csvFilm)));
                while ((line = br2.readLine()) != null) {
                    String[] film = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    Saal s = saals.get(rand.nextInt(saals.size()));
                    Film f = new Film(new Date(), new Date(), s, m);
                    m.addFilm(f);
                    s.addFilm(f);
                    proxy.getDao().createFilm(f);
                }
            }


            System.out.println("done movie and film");
            System.out.println("start person and ticket");

            br = new BufferedReader(new InputStreamReader(DataFiler.class.getClassLoader().getResourceAsStream(csvPerson)));
            List<Film> filmList = proxy.getDao().getallFilmsWithTickets();
            List<Person> persons = new ArrayList<>();
            int count = 0;
            while ((line = br.readLine()) != null) {
                String[] person = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                String email = person[0];
                String firstname = person[1];
                String surname = person[2];
                String country = person[3];
                String location = person[4];
                String zipcode = person[5];
                String street = person[6];
                String passwordHash = person[6];

                Person p = new Person(email, firstname, surname, country, location, zipcode, street, passwordHash);

                if (persons.size() > 2){
                    p.addFriend(persons.get(count-1));
                }
                try {
                    proxy.getDao().createPerson(p);
                    persons.add(p);
                    count++;
                }catch (Exception e){
                    
                }

                br2 = new BufferedReader(new InputStreamReader(DataFiler.class.getClassLoader().getResourceAsStream(csvTicket)));
                while ((line = br2.readLine()) != null) {
                    String[] ticket = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    String creditcard = ticket[0];
                    String paymentdate = ticket[1];

                    Film film = filmList.get(rand.nextInt(filmList.size()));
                    Ticket t = new Ticket(creditcard, new Date(), film, p);
                    try {
                        film.addTicket(t);
                    }catch (Exception e){
                        continue;
                    }
                    p.addTicket(t);
                    proxy.getDao().createTicket(t);
                }
            }
            System.out.println("done person and ticket");
            System.out.println("start owner");

            List<Kino> kinoList = proxy.getDao().getallKinosWithOwnersAndMitarbeiter();
            br = new BufferedReader(new InputStreamReader(DataFiler.class.getClassLoader().getResourceAsStream(csvOwner)));
            while ((line = br.readLine()) != null) {
                String[] owner = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                String email = owner[0];
                String firstname = owner[1];
                String surname = owner[2];
                String country = owner[3];
                String location = owner[4];
                String zipcode = owner[5];
                String street = owner[6];
                String passwordHash = owner[7];
                String iban = owner[8];
                String telephone = owner[9];
                Kino k = kinoList.get(rand.nextInt(kinoList.size()));
                Owner o = new Owner(email, firstname, surname, country, location, zipcode, street, passwordHash, iban, telephone);
                o.addKinotopossessions(k);
                k.addOwner(o);
                try {
                    proxy.getDao().createPerson(o.getPerson());
                    proxy.getDao().createOwner(o);
                    proxy.getDao().updateKino(k);
                }catch (Exception e){

                }

            }
            System.out.println("done owner");
            System.out.println("start staff");

            br = new BufferedReader(new InputStreamReader(DataFiler.class.getClassLoader().getResourceAsStream(csvStaff)));
            while ((line = br.readLine()) != null) {
                String[] staff = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                String email = staff[0];
                String firstname = staff[1];
                String surname = staff[2];
                String country = staff[3];
                String location = staff[4];
                String zipcode = staff[5];
                String street = staff[6];
                String passwordHash = staff[7];
                String sex = staff[8];
                String salary = staff[9];

                Kino k = kinoList.get(rand.nextInt(kinoList.size()));
                Staff staff1 = new Staff(email, firstname, surname, country, location, zipcode, street, passwordHash, sex.charAt(0), Double.parseDouble(salary));
                staff1.setWorklocation(k);
                k.addMitarbeiter(staff1);
                try {
                    proxy.getDao().createPerson(staff1.getPerson());
                    proxy.getDao().createStaff(staff1);
                }catch (Exception e){

                }

            }
            System.out.println("done staff");

            System.out.println("Done filling tables");


            } catch(FileNotFoundException e){
                e.printStackTrace();
            } catch(IOException e){
                e.printStackTrace();
        }
    }

    public static void migrateDB(){
        List<Kino> kinoList = proxy.getDao().getallKinos();
        List<Movie> movieList = proxy.getDao().getallMoviesWithFilms();
        List<Person> personList = proxy.getDao().getallPersons();
        List<Staff> staffList = proxy.getDao().getallStaff();
        List<Owner> ownerList = proxy.getDao().getallOwnersWithPossessions();

        proxy.changeToMongoDB();
        proxy.getDao().dropAll();
        try {
            for (Kino k : kinoList) {
                proxy.getDao().createKino(k);
            }
            for (Movie m : movieList) {
                proxy.getDao().createMovie(m);
            }

            for (Person p : personList) {
                proxy.getDao().createPerson(p);
            }

            for (Staff s : staffList) {
                proxy.getDao().createStaff(s);
            }

            for (Owner o : ownerList) {
                proxy.getDao().createOwner(o);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("done");

    }
}
