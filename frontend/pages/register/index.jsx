import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { connect } from 'react-redux';
import Router from 'next/router';
import { Form, Field } from 'react-final-form';
import LinearProgress from '@material-ui/core/LinearProgress';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { apiService } from '../../src/api/ApiService';
import TextField from '../../src/components/form/TextField';

const styles = theme => ({
  main: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

const validate = values => {
  const errors = {};
  if (!values.passwordhash) {
    errors.passwordhash = 'Required';
  }
  if (!values.email) {
    errors.email = 'Required';
  }
  return errors;
};

class Register extends React.Component {
  state = {
    isLoading: false,
    error: null,
    open: false
  };

  componentDidUpdate() {
    const { details } = this.props;
    if (details !== null) {
      Router.push('/movies');
    }
  }

  handleSnackbarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  registerUser = user => {
    apiService({
      url: `/rest/user/add`,
      method: 'post',
      data: user
    })
      .then(result => {
        if (result.code !== 4) {
          this.setState({
            error: 'statuscode != 4',
            isLoading: false
          });
        } else {
          this.setState({
            open: true
          });
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  };

  render() {
    const { classes } = this.props;
    const { isLoading, error, open } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h6" className={classes.title}>
            {error.message}
          </Typography>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }
    return (
      <main className={classes.main}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          open={open}
          autoHideDuration={1500}
          onClose={this.handleSnackbarClose}
          ContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={<span id="message-id">Success</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleSnackbarClose}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Register
          </Typography>
          <div className={classes.form}>
            <Form
              onSubmit={this.registerUser}
              validate={validate}
              render={({ handleSubmit, submitting, pristine, valid }) => (
                <form onSubmit={handleSubmit}>
                  <Field
                    name="firstname"
                    component={TextField}
                    label="Firstname"
                    type="text"
                    fullWidth
                    autoFocus
                  />
                  <Field
                    name="surname"
                    component={TextField}
                    label="Lastname"
                    type="text"
                    fullWidth
                  />
                  <Field
                    name="country"
                    component={TextField}
                    label="Country"
                    type="text"
                    fullWidth
                  />
                  <Field
                    name="location"
                    component={TextField}
                    label="City"
                    type="text"
                    fullWidth
                  />
                  <Field
                    name="zipcode"
                    component={TextField}
                    label="zip code"
                    type="text"
                    fullWidth
                  />
                  <Field
                    name="street"
                    component={TextField}
                    label="Street"
                    type="text"
                    fullWidth
                  />
                  <Field
                    name="email"
                    component={TextField}
                    label="Email"
                    type="email"
                    required
                    fullWidth
                  />
                  <Field
                    name="passwordhash"
                    component={TextField}
                    label="Password"
                    type="password"
                    required
                    fullWidth
                  />
                  <div className="buttons">
                    <Button
                      type="submit"
                      disabled={submitting || pristine || !valid}
                    >
                      Register
                    </Button>
                    <Button onClick={() => Router.push('/login')}>Login</Button>
                  </div>
                </form>
              )}
            />
          </div>
        </Paper>
      </main>
    );
  }
}

Register.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { details } = state.user;
  return { details };
};

export default connect(mapStateToProps)(withStyles(styles)(Register));
