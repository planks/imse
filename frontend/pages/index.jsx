/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 20
  }
});

const Index = ({ classes }) => (
  <div className={classes.root}>
    <Typography variant="h4" gutterBottom>
      Cineplexx
    </Typography>
    <Typography variant="subtitle1" gutterBottom>
      IMSE project
    </Typography>
  </div>
);

export default withStyles(styles)(Index);
