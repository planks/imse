import React from 'react';
import App, { Container } from 'next/app';
import Head from 'next/head';
import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import JssProvider from 'react-jss/lib/JssProvider';
import Router from 'next/router';
import getPageContext from '../src/getPageContext';
import Layout from '../layouts';
import { initStore } from '../src/store';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    const { user } = ctx.store.getState();
    // console.log(user.details, ctx.asPath);
    if (
      user.details === null &&
      !['/register', '/login'].includes(ctx.asPath)
    ) {
      if (ctx.res) {
        ctx.res.writeHead(302, {
          Location: '/login'
        });
        ctx.res.end();
      } else {
        Router.push('/login');
      }
    } else if (
      user.details !== null &&
      ['/register', '/login'].includes(ctx.asPath)
    ) {
      if (ctx.res) {
        ctx.res.writeHead(302, {
          Location: '/movies'
        });
        ctx.res.end();
      } else {
        Router.push('/movies');
      }
    }

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps: { ...pageProps } };
  }

  constructor() {
    super();
    this.pageContext = getPageContext();
  }

  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  renderWithLayout() {
    const { Component, pageProps } = this.props;

    return (
      <Layout pageContext={this.pageContext} {...pageProps}>
        <Component {...pageProps} />
      </Layout>
    );
  }

  renderWithoutLayout() {
    const { Component, pageProps } = this.props;

    return <Component pageContext={this.pageContext} {...pageProps} />;
  }

  render() {
    const { store, router } = this.props;
    return (
      <Container>
        <Head>
          <title> My page </title>
        </Head>
        <Provider store={store}>
          {/* Wrap every page in Jss and Theme providers */}
          <JssProvider
            registry={this.pageContext.sheetsRegistry}
            generateClassName={this.pageContext.generateClassName}
          >
            {/* MuiThemeProvider makes the theme available down the React
                      tree thanks to React context. */}
            <MuiThemeProvider
              theme={this.pageContext.theme}
              sheetsManager={this.pageContext.sheetsManager}
            >
              {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
              <CssBaseline />
              {/* Pass pageContext to the _document though the renderPage enhancer
                        to render collected styles on server-side. */}
              {['/register', '/login'].includes(router.pathname)
                ? this.renderWithoutLayout()
                : this.renderWithLayout()}
            </MuiThemeProvider>
          </JssProvider>
        </Provider>
      </Container>
    );
  }
}

export default withRedux(initStore)(MyApp);
