import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Form, Field } from 'react-final-form';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import LinearProgress from '@material-ui/core/LinearProgress';
import TextField from '../../src/components/form/TextField';
import { apiService } from '../../src/api/ApiService';
import { setUserData } from '../../src/redux/user';

const styles = theme => ({
  main: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginTop: theme.spacing.unit * 3,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  }
});

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const onSubmit = async values => {
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};
const validate = () => {
  const errors = {};
  return errors;
};

class MyProfile extends React.Component {
  state = {
    open: false,
    isLoading: false,
    error: null
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleClickDelete = () => {
    // TODO: send delete thunk to BE
    window.location.reload();
    // this.setState({ open: false });
  };

  updateProfile = values => {
    const { userData } = this.props;

    apiService({
      url: `/rest/user/updatePerson`,
      method: 'PUT',
      data: {
        ...userData,
        ...values
      }
    })
      .then(result => {
        if (result.code !== 4) {
          this.setState({
            error: 'statuscode != 4',
            isLoading: false
          });
        } else {
          this.props.setUserData({
            ...userData,
            ...values
          });
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  };

  render() {
    const { classes, userData } = this.props;
    const { open, error, isLoading } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h6" className={classes.title}>
            {error.message}
          </Typography>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }

    return (
      <div className={classes.main}>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Are you sure?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              This will delete your account and remove all your data from our
              system.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              No
            </Button>
            <Button onClick={this.handleClickDelete} color="primary">
              Yes
            </Button>
          </DialogActions>
        </Dialog>
        <Typography variant="h4" gutterBottom>
          Edit my Profile
        </Typography>
        <Paper className={classes.paper}>
          <Form
            onSubmit={onSubmit}
            validate={validate}
            initialValues={{
              firstname: userData ? userData.firstname : '',
              surname: userData ? userData.surname : '',
              country: userData ? userData.country : '',
              location: userData ? userData.location : '',
              zipcode: userData ? userData.zipcode : '',
              street: userData ? userData.street : ''
            }}
            render={({ handleSubmit, submitting, pristine, values, valid }) => (
              <form onSubmit={handleSubmit}>
                <Field
                  name="firstname"
                  component={TextField}
                  label="firstname"
                  type="text"
                  fullWidth
                  autoFocus
                />
                <Field
                  name="surname"
                  component={TextField}
                  label="surname"
                  type="text"
                  fullWidth
                />
                <Field
                  name="country"
                  component={TextField}
                  label="Country"
                  type="text"
                  fullWidth
                />
                <Field
                  name="location"
                  component={TextField}
                  label="location"
                  type="text"
                  fullWidth
                />
                <Field
                  name="zipcode"
                  component={TextField}
                  label="zip code"
                  type="text"
                  fullWidth
                />
                <Field
                  name="street"
                  component={TextField}
                  label="Street"
                  type="text"
                  fullWidth
                />
                <div className="buttons">
                  <Button
                    onClick={() => this.updateProfile(values)}
                    disabled={submitting || pristine || !valid}
                  >
                    Update Profile
                  </Button>
                  <Button onClick={this.handleClickOpen}>Delete User</Button>
                </div>
              </form>
            )}
          />
        </Paper>
      </div>
    );
  }
}

MyProfile.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { details } = state.user;
  return { userData: details };
};

const mapDispatchToProps = dispatch => ({
  setUserData: data => dispatch(setUserData(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(MyProfile));
