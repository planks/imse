/* eslint no-shadow: 0 */ // --> OFF

import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { Form, Field } from 'react-final-form';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';
import { withRouter } from 'next/router';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { OnChange } from 'react-final-form-listeners';
import Select from '../../../../src/components/form/Select';
import { apiService } from '../../../../src/api/ApiService';
import DateTimePicker from '../../../../src/components/form/DateTimePicker';

const styles = theme => ({
  main: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginTop: theme.spacing.unit * 3,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  }
});

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const onSubmit = async values => {
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};
const validate = () => {
  const errors = {};
  return errors;
};

class EditScreening extends React.Component {
  state = {
    isLoading: false,
    error: null,
    open: false,
    screening: null,
    halls: [],
    movies: [],
    cinemas: []
  };

  async componentDidMount() {
    const { router } = this.props;

    this.setState({ isLoading: true });

    const p1 = apiService({
      url: `/rest/film/${router.query.id}`,
      method: 'GET'
    });

    const p2 = apiService({
      url: `/rest/movie`,
      method: 'GET'
    });

    const p3 = apiService({
      url: `/rest/kino`,
      method: 'GET'
    });

    try {
      const [screening, movies, cinemas] = await Promise.all([p1, p2, p3]);
      this.setState({
        cinemas: JSON.parse(cinemas.message),
        screening: JSON.parse(screening.message),
        movies: JSON.parse(movies.message)
      });

      const halls = await apiService({
        url: `/rest/kino/${
          JSON.parse(screening.message).saal.kinolocation.id
        }/saal`,
        method: 'GET'
      });

      this.setState({
        halls: JSON.parse(halls.message),
        isLoading: false
      });
    } catch (error) {
      this.setState({
        error,
        isLoading: false
      });
    }
  }

  getInitialCinemaValue(screening) {
    if (screening && screening.saal && screening.saal.kinolocation) {
      return this.state.cinemas.find(
        cinema => cinema.id === screening.saal.kinolocation.id
      );
    }

    return '';
  }

  getInitialHallValue(screening) {
    if (screening && screening.saal && screening.saal.kinolocation) {
      return this.state.halls.find(
        haal => haal.kinolocation.id === screening.saal.kinolocation.id
      );
    }

    return '';
  }

  getInitialMovieValue(screening) {
    if (screening && screening.movie) {
      return this.state.movies.find(movie => movie.id === screening.movie.id);
    }

    return '';
  }

  handleSnackbarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  editScreening = screening => {
    const { router } = this.props;
    apiService({
      url: `/rest/film/`,
      method: 'PUT',
      data: {
        id: router.query.id,
        beginn:
          typeof screening.beginn === 'string'
            ? new Date(screening.beginn)
            : screening.beginn,
        end:
          typeof screening.end === 'string'
            ? new Date(screening.end)
            : screening.end,
        saal: screening.hall,
        movie: screening.movie
      }
    })
      .then(result => {
        if (result.code !== 4) {
          this.setState({
            error: { message: 'statuscode != 4' },
            isLoading: false
          });
        } else {
          this.setState({
            open: true
          });
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  };

  render() {
    const { classes } = this.props;
    const {
      screening,
      halls,
      isLoading,
      error,
      open,
      movies,
      cinemas
    } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h6" className={classes.title}>
            {error.message}
          </Typography>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }

    return (
      <div className={classes.main}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          open={open}
          autoHideDuration={1500}
          onClose={this.handleSnackbarClose}
          ContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={<span id="message-id">Success</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleSnackbarClose}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
        <Typography variant="h4" gutterBottom>
          Edit Screening
        </Typography>
        <Paper className={classes.paper}>
          <Form
            onSubmit={onSubmit}
            validate={validate}
            initialValues={{
              beginn: screening ? screening.beginn : '',
              end: screening ? screening.end : '',
              kino: screening ? this.getInitialCinemaValue(screening) : '',
              hall: screening ? this.getInitialHallValue(screening) : '',
              movie: screening ? this.getInitialMovieValue(screening) : ''
            }}
            render={({ handleSubmit, submitting, pristine, values, valid }) => (
              <form onSubmit={handleSubmit}>
                <Field
                  name="kino"
                  component={Select}
                  options={cinemas}
                  label="Cinema"
                  type="text"
                  fullWidth
                />
                <OnChange name="kino">
                  {(value, previous) => {
                    if (value === previous) return;
                    apiService({
                      url: `/rest/kino/${value.id}/saal`,
                      method: 'GET'
                    })
                      .then(result => {
                        if (result.code === 4) {
                          this.setState({
                            halls: JSON.parse(result.message)
                          });
                        } else {
                          this.setState({
                            error: 'statuscode != 4'
                          });
                        }
                      })
                      .catch(error =>
                        this.setState({
                          error
                        })
                      );
                  }}
                </OnChange>
                <Field
                  name="hall"
                  component={Select}
                  options={halls}
                  label="Hall"
                  type="text"
                  fullWidth
                />
                <Field
                  name="movie"
                  component={Select}
                  options={movies}
                  label="Movie"
                  type="text"
                  fullWidth
                />
                <Field
                  name="beginn"
                  component={DateTimePicker}
                  label="Begin time"
                  fullWidth
                />
                <Field
                  name="end"
                  component={DateTimePicker}
                  label="End time"
                  fullWidth
                />
                <div className="buttons">
                  <Button
                    onClick={() => this.editScreening(values)}
                    disabled={submitting || pristine || !valid}
                  >
                    Edit Screening
                  </Button>
                </div>
              </form>
            )}
          />
        </Paper>
      </div>
    );
  }
}

EditScreening.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(withRouter(EditScreening));
