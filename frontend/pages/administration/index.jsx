import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import PersonIcon from '@material-ui/icons/Person';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import MovieIcon from '@material-ui/icons/Movie';
import Link from 'next/link';
import LinearProgress from '@material-ui/core/LinearProgress';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { apiService } from '../../src/api/ApiService';
// import VirtualList from 'react-virtual-list';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 0 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  bottomNav: {
    position: 'fixed',
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: theme.palette.background.paper,
    zIndex: 100
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 8,
    right: theme.spacing.unit * 2,
    zIndex: 100
  }
});

class MoviesAdministration extends React.Component {
  state = {
    tab: 0,
    isLoading: false,
    error: null,
    movies: [],
    screenings: [],
    users: []
  };

  componentDidMount() {
    this.loadData();
  }

  handleChange = (event, tab) => {
    this.setState({ tab });
  };

  async loadData() {
    this.setState({ isLoading: true });

    const p1 = apiService({
      url: `/rest/film`,
      method: 'GET'
    });

    const p2 = apiService({
      url: `/rest/movie`,
      method: 'GET'
    });

    const p3 = apiService({
      url: `/rest/friend/personlist`,
      method: 'GET'
    });
    try {
      const [screenings, movies, users] = await Promise.all([p1, p2, p3]);
      this.setState({
        users: JSON.parse(users.message),
        screenings: JSON.parse(screenings.message),
        movies: JSON.parse(movies.message),
        isLoading: false
      });
    } catch (error) {
      this.setState({
        error,
        isLoading: false
      });
    }
  }

  removeUser(email) {
    const newState = this.state;
    const index = newState.users.findIndex(a => a === email);

    if (index === -1) return;

    apiService({
      url: `/rest/user/${email}`,
      method: 'DELETE'
    })
      .then(result => {
        if (result.code === 4) {
          this.loadData();
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  removeMovie(id) {
    const newState = this.state;
    const index = newState.movies.findIndex(a => a.id === id);
    if (index === -1) return;

    apiService({
      url: `/rest/movie/${id}`,
      method: 'DELETE'
    })
      .then(result => {
        if (result.code === 4) {
          this.loadData();
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  removeScreening(id) {
    const newState = this.state;
    const index = newState.screenings.findIndex(a => a.id === id);
    if (index === -1) return;

    apiService({
      url: `/rest/film/${id}`,
      method: 'DELETE'
    })
      .then(result => {
        if (result.code === 4) {
          this.loadData();
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  renderMovieList() {
    const { movies } = this.state;
    const { classes } = this.props;
    return (
      <React.Fragment>
        <List>
          {movies.map(movie => (
            <React.Fragment key={movie.id}>
              <ListItem>
                <ListItemText
                  primary={movie.title}
                  secondary={`Genre: ${movie.genre} Rating: ${movie.rating}`}
                />
                <ListItemSecondaryAction>
                  <IconButton
                    aria-label="Delete"
                    onClick={() => this.removeMovie(movie.id)}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <Link href={`/administration/movies/edit?id=${movie.id}`}>
                    <IconButton aria-label="Edit">
                      <EditIcon />
                    </IconButton>
                  </Link>
                </ListItemSecondaryAction>
              </ListItem>
              <Divider />
            </React.Fragment>
          ))}
        </List>
        <Link href="/administration/movies/new">
          <Fab className={classes.fab} color="primary">
            <AddIcon />
          </Fab>
        </Link>
      </React.Fragment>
    );
  }

  renderScreeningsList() {
    const { screenings } = this.state;
    const { classes } = this.props;

    return (
      <React.Fragment>
        <List>
          {screenings.map(screening => (
            <React.Fragment key={screening.id}>
              <ListItem>
                <ListItemText
                  primary={`Begin: ${screening.beginn} End:${screening.end}`}
                  secondary={`Movie: ${screening.movie.title} Location: ${
                    screening.saal.kinolocation.name
                  } Haal: ${screening.saal.saalnumber}`}
                />{' '}
                <ListItemSecondaryAction>
                  <IconButton
                    aria-label="Delete"
                    onClick={() => this.removeScreening(screening.id)}
                  >
                    <DeleteIcon />
                  </IconButton>
                  <Link
                    href={`/administration/screenings/edit?id=${screening.id}`}
                  >
                    <IconButton aria-label="Edit">
                      <EditIcon />
                    </IconButton>
                  </Link>
                </ListItemSecondaryAction>
              </ListItem>
              <Divider />
            </React.Fragment>
          ))}
        </List>
        <Link href="/administration/screenings/new">
          <Fab className={classes.fab} color="primary">
            <AddIcon />
          </Fab>
        </Link>
      </React.Fragment>
    );
  }

  renderUsersList() {
    const { users } = this.state;
    return (
      <List>
        {users.map(user => (
          <React.Fragment key={user}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <PersonIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={user} />
              <ListItemSecondaryAction>
                <Button onClick={() => this.removeUser(user)}>Delete</Button>
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
          </React.Fragment>
        ))}
      </List>
    );
  }

  render() {
    const { classes } = this.props;
    const { tab, isLoading, error } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h6" className={classes.title}>
            {error.message}
          </Typography>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }

    return (
      <div className={classes.root}>
        <BottomNavigation
          value={tab}
          onChange={this.handleChange}
          showLabels
          className={classes.bottomNav}
        >
          <BottomNavigationAction label="Movies" icon={<MovieIcon />} />
          <BottomNavigationAction label="Screenings" icon={<RestoreIcon />} />
          <BottomNavigationAction label="Users" icon={<PersonIcon />} />
        </BottomNavigation>
        {tab === 0 && <TabContainer>{this.renderMovieList()}</TabContainer>}
        {tab === 1 && (
          <TabContainer>{this.renderScreeningsList()}</TabContainer>
        )}
        {tab === 2 && <TabContainer>{this.renderUsersList()}</TabContainer>}
      </div>
    );
  }
}

MoviesAdministration.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MoviesAdministration);
