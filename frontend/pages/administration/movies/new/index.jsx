/* eslint no-shadow: 0 */ // --> OFF

import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { Form, Field } from 'react-final-form';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';
import { withRouter } from 'next/router';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Select from '../../../../src/components/form/Select';
import TextField from '../../../../src/components/form/TextField';
import { apiService } from '../../../../src/api/ApiService';

const styles = theme => ({
  main: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginTop: theme.spacing.unit * 3,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  }
});

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const onSubmit = async values => {
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};
const validate = () => {
  const errors = {};
  return errors;
};

class CreateMovie extends React.Component {
  state = {
    isLoading: false,
    error: null,
    movie: [],
    open: false
  };

  handleSnackbarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  updateMovie = movie => {
    apiService({
      url: `/rest/movie/`,
      method: 'POST',
      data: movie
    })
      .then(result => {
        if (result.code !== 4) {
          this.setState({
            error: { message: 'statuscode != 4' },
            isLoading: false
          });
        } else {
          this.setState({
            open: true
          });
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  };

  render() {
    const { classes } = this.props;
    const { movie, isLoading, error, open } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h6" className={classes.title}>
            {error.message}
          </Typography>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }

    return (
      <div className={classes.main}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          open={open}
          autoHideDuration={1500}
          onClose={this.handleSnackbarClose}
          ContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={<span id="message-id">Success</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleSnackbarClose}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
        <Typography variant="h4" gutterBottom>
          Create Movie
        </Typography>
        <Paper className={classes.paper}>
          <Form
            onSubmit={onSubmit}
            validate={validate}
            initialValues={{
              title: movie ? movie.title : '',
              genre: movie ? movie.genre : '',
              rating: movie ? movie.rating : ''
            }}
            render={({ handleSubmit, submitting, pristine, values, valid }) => (
              <form onSubmit={handleSubmit}>
                <Field
                  name="title"
                  component={TextField}
                  label="Title"
                  type="text"
                  fullWidth
                  autoFocus
                />
                <Field
                  name="genre"
                  component={Select}
                  label="Genre"
                  options={[
                    { value: 'Action', text: 'Action' },
                    { value: 'SciFi', text: 'SciFi' },
                    { value: 'Love', text: 'Love' },
                    { value: 'Fantasy', text: 'Fantasy' },
                    { value: 'Thriller', text: 'Thriller' },
                    { value: 'Horror', text: 'Horror' }
                  ]}
                  fullWidth
                />
                <Field
                  name="rating"
                  component={Select}
                  options={[
                    { value: 0, text: 0 },
                    { value: 1, text: 1 },
                    { value: 2, text: 2 },
                    { value: 3, text: 3 },
                    { value: 4, text: 4 },
                    { value: 5, text: 5 }
                  ]}
                  label="Rating"
                  type="text"
                  id="rating-select"
                  fullWidth
                />
                <div className="buttons">
                  <Button
                    onClick={() => this.updateMovie(values)}
                    disabled={submitting || pristine || !valid}
                  >
                    Create Movie
                  </Button>
                </div>
              </form>
            )}
          />
        </Paper>
      </div>
    );
  }
}

CreateMovie.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(withRouter(CreateMovie));
