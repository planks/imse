import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { Field } from 'react-final-form';
import TextField from '../../src/components/form/TextField';

function PaymentForm() {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Payment method
      </Typography>
      <Grid container spacing={24}>
        <Grid item xs={12} md={6}>
          <Field
            name="cardName"
            component={TextField}
            label="Name on card"
            type="text"
            fullWidth
            autoFocus
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="cardNumber"
            component={TextField}
            label="Card number"
            type="text"
            fullWidth
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="expDate"
            component={TextField}
            label="Expiry date"
            type="text"
            fullWidth
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="cvv"
            component={TextField}
            label="CVV"
            helperText="Last three digits on signature strip"
            type="text"
            fullWidth
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default PaymentForm;
