import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Form } from 'react-final-form';
import LinearProgress from '@material-ui/core/LinearProgress';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import PaymentForm from './PaymentForm';
import Review from './Review';
import { apiService } from '../../src/api/ApiService';

const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3,
    padding: theme.spacing.unit * 2,
    [theme.breakpoints.up(600 + theme.spacing.unit * 3 * 2)]: {
      marginTop: theme.spacing.unit * 6,
      marginBottom: theme.spacing.unit * 6,
      padding: theme.spacing.unit * 3
    }
  },
  stepper: {
    padding: `${theme.spacing.unit * 3}px 0 ${theme.spacing.unit * 5}px`
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  button: {
    marginTop: theme.spacing.unit * 3,
    marginLeft: theme.spacing.unit
  }
});

const steps = ['Payment details', 'Review your order'];

function getStepContent(step, values, screening) {
  switch (step) {
    case 0:
      return <PaymentForm />;
    case 1:
      return <Review values={values} screening={screening} />;
    default:
      throw new Error('Unknown step');
  }
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const onSubmit = async values => {
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};

const validate = values => {
  const errors = {};
  if (!values.password) {
    errors.password = 'Required';
  }
  if (!values.email) {
    errors.email = 'Required';
  }
  return errors;
};

class Checkout extends React.Component {
  state = {
    activeStep: 0,
    screening: null,
    isLoading: false,
    error: null,
    ticketNumber: null
  };

  componentDidMount() {
    const { router } = this.props;

    this.setState({ isLoading: true });

    apiService({
      url: `/rest/film/${router.query.id}`,
      method: 'GET'
    })
      .then(result => {
        if (result.code === 4) {
          this.setState({
            screening: JSON.parse(result.message),
            isLoading: false
          });
        } else {
          this.setState({
            error: 'statuscode != 4',
            isLoading: false
          });
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  handleNext = values => {
    const { router, userData } = this.props;
    const { screening } = this.state;

    if (this.state.activeStep === 1) {
      console.log('buy ticket');
      this.setState({ isLoading: true });
      const date = new Date();
      apiService({
        url: `/rest/film/${router.query.id}/buyticket`,
        method: 'POST',
        data: {
          creditcardnumber: values.cardNumber,
          paymentdate: date.toISOString(),
          film: {
            id: screening.id
          },
          owner: {
            email: userData.email
          }
        }
      })
        .then(result => {
          if (result.code !== 4) {
            this.setState({
              error: { message: 'statuscode != 4' },
              isLoading: false
            });
          } else {
            this.setState(state => ({
              activeStep: state.activeStep + 1,
              isLoading: false,
              ticketNumber: result.message
            }));
          }
        })
        .catch(error =>
          this.setState({
            error,
            isLoading: false
          })
        );
    } else {
      this.setState(state => ({
        activeStep: state.activeStep + 1
      }));
    }
  };

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1
    }));
  };

  handleReset = () => {
    this.setState({
      activeStep: 0
    });
  };

  render() {
    const { classes } = this.props;
    const {
      activeStep,
      screening,
      isLoading,
      error,
      ticketNumber
    } = this.state;

    if (error) {
      return (
        <div className={classes.layout}>
          <Paper className={classes.paper}>
            <Typography variant="h6" className={classes.title}>
              {error.message}
            </Typography>
          </Paper>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }

    return (
      <React.Fragment>
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h4" align="center">
              Checkout
            </Typography>
            <Stepper activeStep={activeStep} className={classes.stepper}>
              {steps.map(label => (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <React.Fragment>
              {activeStep === steps.length ? (
                <React.Fragment>
                  <Typography variant="h5" gutterBottom>
                    Thank you for your order.
                  </Typography>
                  <Typography variant="subtitle1">
                    Your order number is {ticketNumber}. We have emailed your
                    order confirmation with a code you can use to pick up your
                    ticket in the cinema.
                  </Typography>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <Form
                    onSubmit={onSubmit}
                    validate={validate}
                    render={({ handleSubmit, values }) => (
                      <form onSubmit={handleSubmit}>
                        {getStepContent(activeStep, values, screening)}
                        <div className={classes.buttons}>
                          {activeStep !== 0 && (
                            <Button
                              onClick={this.handleBack}
                              className={classes.button}
                            >
                              Back
                            </Button>
                          )}
                          <Button
                            variant="contained"
                            color="primary"
                            onClick={() => this.handleNext(values)}
                            className={classes.button}
                          >
                            {activeStep === steps.length - 1
                              ? 'Place order'
                              : 'Next'}
                          </Button>
                        </div>
                      </form>
                    )}
                  />
                </React.Fragment>
              )}
            </React.Fragment>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

Checkout.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { details } = state.user;
  return { userData: details };
};

export default connect(mapStateToProps)(
  withStyles(styles)(withRouter(Checkout))
);
