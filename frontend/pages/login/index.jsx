/* eslint no-shadow: 0 */ // --> OFF

import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import LockIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { connect } from 'react-redux';
import Router from 'next/router';
import { Form, Field } from 'react-final-form';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '../../src/components/form/TextField';
import { setUserData } from '../../src/redux/user';
import { apiService } from '../../src/api/ApiService';

const styles = theme => ({
  main: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    height: '100vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

const validate = values => {
  const errors = {};
  if (!values.password) {
    errors.password = 'Required';
  }
  if (!values.email) {
    errors.email = 'Required';
  }
  return errors;
};
class Login extends React.Component {
  state = {
    open: false
  };

  componentDidUpdate() {
    const { details } = this.props;
    if (details !== null) {
      Router.push('/movies');
    }
  }

  loginUser = async user => {
    const { setUserData } = this.props;

    try {
      const result = await apiService({
        url: `/rest/user/testLogin/${user.email}/${user.password}`,
        method: 'GET'
      });
      if (result.message === 'null') {
        this.setState({
          open: true
        });
      } else {
        setUserData(JSON.parse(result.message));
      }
    } catch (error) {
      this.setState({
        open: true
      });
    }
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <main className={classes.main}>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          open={open}
          autoHideDuration={1500}
          onClose={this.handleSnackbarClose}
          ContentProps={{
            'aria-describedby': 'message-id'
          }}
          message={<span id="message-id">Login Error</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              className={classes.close}
              onClick={this.handleSnackbarClose}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <div className={classes.form}>
            <Form
              onSubmit={this.loginUser}
              validate={validate}
              render={({ handleSubmit, submitting, pristine, valid }) => (
                <form onSubmit={handleSubmit}>
                  <Field
                    name="email"
                    component={TextField}
                    label="Email"
                    type="email"
                    fullWidth
                    autoFocus
                  />
                  <Field
                    name="password"
                    label="Password"
                    component={TextField}
                    autoComplete="current-password"
                    type="password"
                    fullWidth
                  />
                  <div className="buttons">
                    <Button
                      type="submit"
                      disabled={submitting || pristine || !valid}
                    >
                      Login
                    </Button>
                    <Button onClick={() => Router.push('/register')}>
                      Register
                    </Button>
                  </div>
                </form>
              )}
            />
          </div>
        </Paper>
      </main>
    );
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { details } = state.user;
  return { details };
};

const mapDispatchToProps = dispatch => ({
  setUserData: data => dispatch(setUserData(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Login));
