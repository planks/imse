/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import BookIcon from '@material-ui/icons/Book';
import DeleteIcon from '@material-ui/icons/Delete';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import LinearProgress from '@material-ui/core/LinearProgress';
import { connect } from 'react-redux';
import { apiService } from '../../src/api/ApiService';

const styles = theme => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 2
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
    zIndex: 100
  },
  appBar: {
    position: 'relative'
  }
});

class MyTickets extends React.Component {
  state = {
    tickets: [],
    isLoading: false,
    error: null
  };

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    this.setState({ isLoading: true });
    const { userData } = this.props;
    apiService({
      url: `/rest/user/${userData.email}/ticket`,
      method: 'GET'
    })
      .then(result => {
        if (result.code === 4) {
          this.setState({
            tickets: JSON.parse(result.message),
            isLoading: false
          });
        } else if (result.code === undefined) {
          this.setState({
            error: 'You have no tickets',
            isLoading: false
          });
        } else {
          this.setState({
            error: 'statuscode != 4',
            isLoading: false
          });
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  removeTicket(id) {
    const newState = this.state;
    const index = newState.tickets.findIndex(ticket => ticket.id === id);

    if (index === -1) return;

    apiService({
      url: `/rest/ticket/${id}`,
      method: 'DELETE'
    })
      .then(result => {
        if (result.code === 4) {
          this.loadData();
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  render() {
    const { classes } = this.props;
    const { tickets, isLoading, error } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h6" className={classes.title}>
            {error.message || error.toString()}
          </Typography>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }

    return (
      <div className={classes.root}>
        <Typography variant="h6" className={classes.title}>
          My Tickets
        </Typography>
        <div>
          <List>
            {tickets.map(ticket => (
              <React.Fragment key={ticket.id}>
                <ListItem>
                  <ListItemAvatar>
                    <Avatar>
                      <BookIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText
                    primary={`Movie: ${ticket.film.movie.title}`}
                    secondary={`Begins: ${ticket.film.beginn}`}
                  />
                  <ListItemSecondaryAction>
                    <IconButton
                      aria-label="Delete"
                      onClick={() => this.removeTicket(ticket.id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
                <Divider />
              </React.Fragment>
            ))}
          </List>
        </div>
      </div>
    );
  }
}

MyTickets.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { details } = state.user;
  return { userData: details };
};

export default connect(mapStateToProps)(withStyles(styles)(MyTickets));
