import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import PersonIcon from '@material-ui/icons/Person';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import LinearProgress from '@material-ui/core/LinearProgress';
import { connect } from 'react-redux';
import { apiService } from '../../src/api/ApiService';

const styles = theme => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 2
  },
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
    zIndex: 100
  },
  appBar: {
    position: 'relative'
  }
});

class MyFriends extends React.Component {
  state = {
    open: false,
    isLoading: false,
    error: null,
    friends: [],
    nonFriends: []
  };

  componentDidMount() {
    this.loadData();
  }

  handleClose = () => {
    this.setState(
      {
        open: false,
        friends: [],
        nonFriends: []
      },
      this.loadData
    );
  };

  handleClick = () => {
    this.setState({
      open: true
    });
  };

  async loadData() {
    this.setState({ isLoading: true });
    const { userData } = this.props;

    const p1 = apiService({
      url: `/rest/friend/friendList/${userData.email}`,
      method: 'GET'
    });

    const p2 = apiService({
      url: `/rest/friend/personlist`,
      method: 'GET'
    });

    try {
      const [friends, persons] = await Promise.all([p1, p2]);
      this.setState({
        friends: JSON.parse(friends.message)
      });

      const nonFriends = JSON.parse(persons.message).filter(
        person => JSON.parse(friends.message).indexOf(person) < 0
      );
      this.setState({
        nonFriends,
        isLoading: false
      });
    } catch (error) {
      this.setState({
        error,
        isLoading: false
      });
    }
  }

  removeFriend(id) {
    const newState = this.state;
    const index = newState.friends.findIndex(a => a.id === id);

    if (index === -1) return;
    newState.friends.splice(index, 1);

    this.setState(newState); // This will update the state and trigger a rerender of the components
  }

  addFriend(mail) {
    const { userData } = this.props;

    apiService({
      url: `/rest/friend/add/${mail}/${userData.email}`,
      method: 'post'
    })
      .then(() => this.handleClose())
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  render() {
    const { classes } = this.props;
    const { open, friends, nonFriends, isLoading, error } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h6" className={classes.title}>
            {error.message}
          </Typography>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }

    return (
      <div className={classes.root}>
        <Dialog open={open} onClose={this.handleClose} fullScreen>
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton
                color="inherit"
                onClick={this.handleClose}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.flex}>
                Add friend
              </Typography>
            </Toolbar>
          </AppBar>
          <List dense>
            {nonFriends.map(person => (
              <React.Fragment key={person}>
                <ListItem>
                  <ListItemText primary={person} />
                  <ListItemSecondaryAction>
                    <IconButton
                      aria-label="add"
                      onClick={() => this.addFriend(person)}
                    >
                      <AddIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
                <Divider />
              </React.Fragment>
            ))}
          </List>
        </Dialog>
        <Typography variant="h6" className={classes.title}>
          Friendlist
        </Typography>
        <div className={classes.demo}>
          <List>
            {friends.map(friend => (
              <React.Fragment key={friend}>
                <ListItem>
                  <ListItemAvatar>
                    <Avatar>
                      <PersonIcon />
                    </Avatar>
                  </ListItemAvatar>
                  <ListItemText primary={friend} />
                  {/* <ListItemSecondaryAction>
                    <IconButton
                      aria-label="Delete"
                      onClick={() => this.removeFriend(friend.id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction> */}
                </ListItem>
                <Divider />
              </React.Fragment>
            ))}
          </List>
          <Fab
            className={classes.fab}
            onClick={this.handleClick}
            color="primary"
          >
            <AddIcon />
          </Fab>
        </div>
      </div>
    );
  }
}

MyFriends.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { details } = state.user;
  return { userData: details };
};

export default connect(mapStateToProps)(withStyles(styles)(MyFriends));
