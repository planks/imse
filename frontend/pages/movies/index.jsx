import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Router from 'next/router';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Form, Field } from 'react-final-form';
import { apiService } from '../../src/api/ApiService';
import Select from '../../src/components/form/Select';
import TextField from '../../src/components/form/TextField';

const styles = theme => ({
  heroContent: {
    maxWidth: 600,
    margin: '0 auto',
    padding: `${theme.spacing.unit * 4}px 0 0`
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  cardGrid: {
    padding: `0`
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardContent: {
    flexGrow: 1
  },
  filter: {
    padding: theme.spacing.unit * 3
  }
});

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const onSubmit = async values => {
  await sleep(300);
  window.alert(JSON.stringify(values, 0, 2));
};

class MovieList extends React.Component {
  state = {
    isLoading: false,
    error: null,
    movies: []
  };

  componentDidMount() {
    this.loadData();
  }

  loadData(values) {
    this.setState({ isLoading: true });
    const title = values && values.title ? `title=${values.title}` : '';
    const rating = values && values.rating ? `&rating=${values.rating}` : '';
    const genre = values && values.genre ? `&genre=${values.genre}` : '';

    const url = title || rating || genre ? '/rest/movie/query?' : '/rest/movie';
    apiService({
      url: url + title + genre + rating,
      method: 'GET'
    })
      .then(result => {
        if (result.code === 4) {
          this.setState({
            movies: JSON.parse(result.message),
            isLoading: false
          });
        } else {
          this.setState({
            error: { message: 'statuscode != 4' },
            isLoading: false
          });
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  render() {
    const { classes } = this.props;
    const { isLoading, error, movies } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h6" className={classes.title}>
            {error.message}
          </Typography>
        </div>
      );
    }

    return (
      <React.Fragment>
        <main>
          {/* Hero unit */}
          <div className={classes.heroUnit}>
            <div className={classes.heroContent}>
              <Typography
                variant="h2"
                component="h1"
                align="center"
                color="textPrimary"
                gutterBottom
              >
                Movie list
              </Typography>
            </div>
          </div>
          <div className={classes.filter}>
            <Form
              onSubmit={onSubmit}
              render={({
                handleSubmit,
                submitting,
                pristine,
                values,
                form,
                valid
              }) => (
                <form onSubmit={handleSubmit}>
                  <Field
                    name="title"
                    component={TextField}
                    label="Title"
                    type="text"
                    fullWidth
                    autoFocus
                  />
                  <Field
                    name="genre"
                    component={Select}
                    label="Genre"
                    options={[
                      { value: 'Action', text: 'Action' },
                      { value: 'SciFi', text: 'SciFi' },
                      { value: 'Love', text: 'Love' },
                      { value: 'Fantasy', text: 'Fantasy' },
                      { value: 'Thriller', text: 'Thriller' },
                      { value: 'Horror', text: 'Horror' }
                    ]}
                    fullWidth
                  />
                  <Field
                    name="rating"
                    component={Select}
                    options={[
                      { value: 0, text: 0 },
                      { value: 1, text: 1 },
                      { value: 2, text: 2 },
                      { value: 3, text: 3 },
                      { value: 4, text: 4 },
                      { value: 5, text: 5 }
                    ]}
                    label="Rating"
                    type="text"
                    id="rating-select"
                    fullWidth
                  />
                  <div className="buttons">
                    <Button
                      onClick={() => this.loadData(values)}
                      disabled={submitting || pristine || !valid}
                    >
                      Apply filters
                    </Button>
                    <Button
                      onClick={() => {
                        form.reset();
                        this.loadData();
                      }}
                      disabled={submitting || pristine}
                    >
                      Reset filters
                    </Button>
                  </div>
                </form>
              )}
            />
          </div>
          {isLoading ? (
            <LinearProgress />
          ) : (
            <div className={classNames(classes.layout, classes.cardGrid)}>
              {/* End hero unit */}
              <Grid container spacing={24}>
                {movies.map(movie => (
                  <Grid item key={movie.id} xs={6} sm={6} md={3} lg={3}>
                    <Card className={classes.card}>
                      <CardContent className={classes.cardContent}>
                        <Typography gutterBottom variant="h5" component="h2">
                          {movie.title}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                          Rating: {movie.rating}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                          Genre: {movie.genre}
                        </Typography>
                      </CardContent>
                      <CardActions>
                        <Button
                          onClick={() =>
                            Router.push(`/movies/details?id=${movie.id}`)
                          }
                          size="small"
                          color="primary"
                        >
                          Screenings
                        </Button>
                      </CardActions>
                    </Card>
                  </Grid>
                ))}
              </Grid>
            </div>
          )}
        </main>
      </React.Fragment>
    );
  }
}

MovieList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MovieList);
