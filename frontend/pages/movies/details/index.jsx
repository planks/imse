/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Router, { withRouter } from 'next/router';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import LinearProgress from '@material-ui/core/LinearProgress';
import { apiService } from '../../../src/api/ApiService';

const styles = theme => ({
  root: {
    textAlign: 'center',
    paddingTop: theme.spacing.unit * 2
  },
  list: {
    width: '100%'
  }
});

class MovieDetails extends React.Component {
  state = {
    isLoading: false,
    error: null,
    screenings: []
  };

  componentDidMount() {
    const { router } = this.props;

    this.setState({ isLoading: true });

    apiService({
      url: `/rest/movie/${router.query.id}/films`,
      method: 'GET'
    })
      .then(result => {
        if (result.code === 4) {
          this.setState({
            screenings: JSON.parse(result.message),
            isLoading: false
          });
        } else if (result.code === undefined) {
          this.setState({
            error: 'There are no screenings for this movie',
            isLoading: false
          });
        } else {
          this.setState({
            error: 'statuscode != 4',
            isLoading: false
          });
        }
      })
      .catch(error =>
        this.setState({
          error,
          isLoading: false
        })
      );
  }

  render() {
    const { classes } = this.props;
    const { screenings, isLoading, error } = this.state;

    if (error) {
      return (
        <div className={classes.root}>
          <Typography variant="h4" gutterBottom className={classes.title}>
            {error.message || error.toString()}
          </Typography>
        </div>
      );
    }
    if (isLoading) {
      return <LinearProgress />;
    }

    return (
      <div className={classes.root}>
        <Typography variant="h4" gutterBottom>
          Screenings
        </Typography>
        <List className={classes.list}>
          {screenings.map(screening => (
            <React.Fragment key={screening.id}>
              <ListItem>
                <ListItemText
                  primary={`Begin: ${screening.beginn} End:${screening.end}`}
                  secondary={`Location: ${
                    screening.saal.kinolocation.name
                  } Haal:${screening.saal.saalnumber}`}
                />
                <ListItemSecondaryAction>
                  <Button
                    onClick={() => Router.push(`/checkout?id=${screening.id}`)}
                    size="small"
                    color="primary"
                  >
                    Buy Ticket
                  </Button>
                </ListItemSecondaryAction>
              </ListItem>
              <Divider />
            </React.Fragment>
          ))}
        </List>
      </div>
    );
  }
}

MovieDetails.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(withRouter(MovieDetails));
