import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { withStyles } from '@material-ui/core/styles';
import Router from 'next/router';
import Button from '@material-ui/core/Button';
import classNames from 'classnames';
import { connect } from 'react-redux';
import LoadingOverlay from 'react-loading-overlay';
import { logout } from '../src/redux/user';
import Drawer from './drawer';

const styles = theme => ({
  appBar: {
    position: 'relative'
  },
  icon: {
    marginRight: theme.spacing.unit * 2
  },
  footer: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    left: 0,
    padding: '1rem',
    backgroundColor: '#efefef',
    textAlign: 'center'
  },
  main: {
    position: 'relative',
    margin: 0,
    paddingBottom: '6rem',
    minHeight: '100vh'
  },
  headerText: {
    cursor: 'pointer'
  },
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  loadingOverlay: {
    height: '100vh'
  }
});

class Header extends React.Component {
  state = {
    open: false,
    loading: false
  };

  toggleDrawer = open => () => {
    this.setState({
      open
    });
  };

  toggleLoading = () => {
    this.setState({
      loading: true
    });
    setTimeout(() => {
      window.location.reload();
    }, 30000);
  };

  logout = async () => {
    // TODO: await logout
    this.props.logout();
    window.location.reload();
  };

  render() {
    const { classes, children } = this.props;
    return (
      <LoadingOverlay
        active={this.state.loading}
        spinner
        text="Switching backend, this takes about 30 seconds. After this the page will reload"
      >
        {/* <style jsx global>{`
          ._loading_overlay_wrapper {
            height: 100vh !important;
            overflow: hidden;
          }
        `}</style> */}
        <div className={classes.main}>
          <AppBar position="static">
            <Toolbar>
              <IconButton
                className={classes.menuButton}
                onClick={this.toggleDrawer(true)}
                color="inherit"
                aria-label="Menu"
              >
                <MenuIcon />
              </IconButton>
              <Typography
                onClick={() => Router.push('/')}
                variant="h6"
                className={classNames(classes.headerText, classes.grow)}
                color="inherit"
                noWrap
              >
                Cineplexx
              </Typography>
              <Button color="inherit" onClick={this.logout}>
                Logout
              </Button>
            </Toolbar>
          </AppBar>
          <Drawer
            open={this.state.open}
            handleDrawerClick={this.toggleDrawer}
            toggleLoading={this.toggleLoading}
          />
          <section>{children}</section>
        </div>
      </LoadingOverlay>
    );
  }
}

const mapStateToProps = state => {
  const { details } = state.user;
  return { details };
};

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Header));
