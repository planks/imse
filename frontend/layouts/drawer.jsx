/* eslint react/prefer-stateless-function: 0 */ // --> OFF

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import BookIcon from '@material-ui/icons/Book';
import MovieIcon from '@material-ui/icons/Movie';
import PersonIcon from '@material-ui/icons/Person';
import EditIcon from '@material-ui/icons/Edit';
import PersonRemoveIcon from '@material-ui/icons/PersonAddDisabled';
import BuildIcon from '@material-ui/icons/Build';
import Link from 'next/link';
import { connect } from 'react-redux';
import Cookie from 'js-cookie';
import { apiService } from '../src/api/ApiService';

const styles = {
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  }
};

class TemporaryDrawer extends React.Component {
  switchBackend = async () => {
    this.props.toggleLoading();
    Cookie.set('mongo', 'true');
    console.log('test');
    try {
      const result = await apiService({
        url: `/rest/switchStorageBackendImplementation`,
        method: 'PUT'
      });
      console.log(result);
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { classes, open, handleDrawerClick, isAdmin } = this.props;
    const mongo = Cookie.get('mongo') === 'true';

    const sideList = (
      <div className={classes.list}>
        <List>
          <Link href="/movies">
            <ListItem button>
              <ListItemIcon>
                <MovieIcon />
              </ListItemIcon>
              <ListItemText primary="Movie list" />
            </ListItem>
          </Link>
          <Link href="/tickets">
            <ListItem button>
              <ListItemIcon>
                <BookIcon />
              </ListItemIcon>
              <ListItemText primary="My tickets" />
            </ListItem>
          </Link>
          <Link href="/friends">
            <ListItem button>
              <ListItemIcon>
                <PersonIcon />
              </ListItemIcon>
              <ListItemText primary="My friends" />
            </ListItem>
          </Link>
        </List>
        {isAdmin && (
          <React.Fragment>
            <Divider />
            <List>
              <Link href="/administration">
                <ListItem button>
                  <ListItemIcon>
                    <PersonRemoveIcon />
                  </ListItemIcon>
                  <ListItemText primary="Administration" />
                </ListItem>
              </Link>
              {!mongo ? (
                <ListItem button onClick={this.switchBackend}>
                  <ListItemIcon>
                    <BuildIcon />
                  </ListItemIcon>
                  <ListItemText primary="Switch Backend" />
                </ListItem>
              ) : (
                <ListItem button disabled>
                  <ListItemIcon>
                    <BuildIcon />
                  </ListItemIcon>
                  <ListItemText primary="Running on mongo" />
                </ListItem>
              )}
            </List>
          </React.Fragment>
        )}

        <Divider />
        <List>
          <Link href="/myprofile">
            <ListItem button>
              <ListItemIcon>
                <EditIcon />
              </ListItemIcon>
              <ListItemText primary="My Profile" />
            </ListItem>
          </Link>
        </List>
      </div>
    );

    return (
      <Drawer open={open} onClose={handleDrawerClick(false)}>
        <div
          tabIndex={0}
          role="button"
          onClick={handleDrawerClick(false)}
          onKeyDown={handleDrawerClick(false)}
        >
          {sideList}
        </div>
      </Drawer>
    );
  }
}

TemporaryDrawer.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { isAdmin } = state.user;
  return { isAdmin };
};

export default connect(mapStateToProps)(withStyles(styles)(TemporaryDrawer));
