import axios from 'axios';
import https from 'https';

const client = axios.create({
  baseURL: 'https://tomcat01lab.cs.univie.ac.at:31113/cineplexx/',
  // baseURL: 'http://localhost:8080/',
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
});

export const apiService = options => {
  const onSuccess = response => response.data;

  const onError = error => {
    let errorMessage = `API request failed for ${error.config.url}: ${
      error.message
    }`;

    if (error.response) {
      // Request was made but server responded with something other than 2xx
      errorMessage += `\n\tStatus: ${error.response.status}`;
      errorMessage += `\n\tData: ${error.response.data}`;
    }

    console.log(error);

    return Promise.reject(new Error(errorMessage));
  };

  return client(options)
    .then(onSuccess)
    .catch(onError);
};
