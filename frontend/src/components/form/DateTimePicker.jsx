import React from 'react';
import { DateTimePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import moment from 'moment';
import MomentUtils from '@date-io/moment';

moment.locale('en');

export default ({
  input: { name, onChange, value, ...restInput },
  meta,
  ...rest
}) => (
  <MuiPickersUtilsProvider utils={MomentUtils} locale="en" moment={moment}>
    <DateTimePicker
      {...rest}
      name={name}
      helperText={meta.touched ? meta.error : undefined}
      error={meta.error && meta.touched}
      inputProps={restInput}
      onChange={onChange}
      value={value}
    />
  </MuiPickersUtilsProvider>
);
