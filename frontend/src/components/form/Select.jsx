/* eslint react/no-array-index-key: 0 */ // --> OFF

import React from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

export default ({
  input: { name, onChange, value, ...restInput },
  meta,
  options,
  id,
  label,
  ...rest
}) => (
  <div>
    <InputLabel htmlFor={id}>
      {meta.touched && meta.error ? meta.error : label}
    </InputLabel>
    <Select
      {...rest}
      name={name}
      error={meta.error && meta.touched}
      inputProps={restInput}
      onChange={onChange}
      value={value}
      id={id}
    >
      {options.map((option, index) => (
        <MenuItem
          key={index}
          value={(option.value === 0 ? '0' : option.value) || option}
        >
          {option.name ||
            option.title ||
            (option.text === 0 ? '0' : option.text) ||
            (option.saalnumber === 0 ? '0' : option.saalnumber)}
        </MenuItem>
      ))}
    </Select>
  </div>
);
