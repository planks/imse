const actions = {
  login: '@@USER/LOGIN',
  logout: '@@USER/LOGOUT'
};

export function setUserData(data) {
  return {
    type: actions.login,
    payload: data
  };
}

export function logout() {
  return {
    type: actions.logout
  };
}

const initialState = {
  details: null,
  isAdmin: false
};

const admins = [
  'dummy1@gmail.com',
  'dummy2@gmail.com',
  'dummy0@gmail.com',
  'thesofty@gmail.com'
];

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case actions.login:
      return {
        ...state,
        details: { ...state.details, ...action.payload },
        isAdmin: admins.includes(action.payload.email)
      };
    case actions.logout:
      return { ...state, details: null };
    default:
      return state;
  }
}
