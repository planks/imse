# IMSE Frontend

- made using next.js
- react
- redux
- material-ui

## How to use

Install it and run:

To run the frontend you need node.js installed on your computer. Then execute in the frontend folder the following commands

```sh
npm install
npm run dev
```

## Backend

The backend url can be configured in /src/api/ApiService
The backend is deployed on https://tomcat01lab.cs.univie.ac.at:31113/cineplexx/
Since a self signed certificate is used for the tomcat server, one has to open the mentioned link and allow the unknown certificate before using the frontend.

## Test

a version of the frontend is deployed at https://imse.snglrty.at/

[Next.js](https://github.com/zeit/next.js) is a framework for server-rendered React apps.
